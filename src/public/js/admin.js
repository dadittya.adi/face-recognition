$(document).ready( function ()
{ 
    $('#passwordEdit').prop('disabled', true);
    var table = $('#dtTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:25,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/backend/admin/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'name', name: 'name',searchable:true,orderable:true},
            {data: 'email', name: 'email',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:true,orderable:true},
        ]
    });

    var dtTable = $('#dtTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtTable.draw();

    $('#form').submit(function (event)
    {
        event.preventDefault();
        var name            = $('#name').val();
        var email           = $('#email').val();
        var password        = $('#password').val();
        var is_modify       = $('#is_modify').val();
       
        if(!name)
        {
            $("#alert_warning").trigger("click", 'Please type name first');
            return false
        }

        if(!email)
        {
            $("#alert_warning").trigger("click", 'Please type email first');
            return false
        }

        if(is_modify == 1 && !password)
        {
            $("#alert_warning").trigger("click", 'Please type password first');
            return false
        }
        
        $('#formModal').modal('toggle');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () 
                    {
                        setToInsert();
                        $("#alert_success").trigger("click", 'Data successfully saved');
                        $('#dtTable').DataTable().ajax.reload();
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        $('#formModal').modal();
                    }
                });
            }
        });
    });
});

$('#email').on('change',function()
{
    var email       = $(this).val();
    var validate    = validateEmail(email);
    
    if(!validate)
    {
        $(this).val('');
        $("#alert_warning").trigger("click",'format email is wrong');
    }
    
});


$("#passwordEdit").on("click", function() 
{
    console.log('asd');
    $('#is_modify').val('1');
    $('#password').attr('readonly', false);
});

function edit(url)
{
    $.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
        success: function () 
        {
			$.unblockUI();
		},
	})
    .done(function (data) 
    {
        $('#title').text('Update Admin');
        $('#name').val(data.name);
        $('#email').val(data.email);
        $('#password').val('');
        $('#password').attr('readonly', true);
        $('#passwordEdit').prop('disabled', false);
        $('#is_modify').val('0');
        $('#form').attr('action', data.url_update);
		$('#formModal').modal();
        
        
	});
}

function setToInsert()
{
    var url_insert =  $('#url_insert').val();
    $('#title').text('Create Admin');
    $('#name').val('');
    $('#email').val('');
    $('#is_modify').val('1');
    $('#password').attr('readonly', false);
    $('#passwordEdit').prop('disabled', true);
    $('#form').attr('action', url_insert);
    $("#formModal .close").click();
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Data successfully deleted');
        $('#dtTable').DataTable().ajax.reload();
    });
}

function validateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}