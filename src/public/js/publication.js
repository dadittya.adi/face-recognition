page         = $('#page').val();

$(document).ready( function ()
{ 
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });

    var message         = $('#message').val();

    if (message == 'success') $("#alert_success").trigger("click", 'Data successfully saved');
    else if (message == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated');


    var table = $('#dtTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:25,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/backend/publication/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'title', name: 'title',searchable:true,orderable:true},
            {data: 'published_date', name: 'published_date',searchable:true,orderable:true},
            {data: 'is_active', name: 'is_active',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:true,orderable:true},
        ]
    });

    var dtTable = $('#dtTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtTable.draw();

    $('#form').submit(function (event)
    {
        event.preventDefault();
        var published_date  = $('#published_date').val();
        var title           = $('#title').val();
        var file            = $('#file').val();
        var abstract        = $('#abstract').val();
        var is_modify       = $('#is_modify').val();
        
        //var is_active       = $('#select_is_active').val();
       
        if(!published_date)
        {
            $("#alert_warning").trigger("click", 'Please select publish date first');
            return false
        }

        if(!title)
        {
            $("#alert_warning").trigger("click", 'Please type title first');
            return false
        }

        if(!abstract)
        {
            $("#alert_warning").trigger("click", 'Please input abstract first');
            return false
        }

        if(is_modify == 1)
        {
            if(!file)
            {
                $("#alert_warning").trigger("click", 'Please upload file first');
                return false
            }
        }
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: new FormData(document.getElementById("form")),
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () 
                    {
                        document.location.href = '/backend/publication';
                        //$("#alert_success").trigger("click", 'Data successfully saved');
                        //$('#dtTable').DataTable().ajax.reload();
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                       
                    }
                });
            }
        });
    });

    if(page == 'edit')
    {
        var is_active   = $('#is_active').val();
        if(is_active == 1) $("#checkbox_is_active").parent().find(".switchery").prop('checked', true).trigger("click");
        $(".file-caption-main").addClass("hidden");
        $("#btn_modify").on("click", function() 
        {
            var txt_btn = $('#text_btn').text();
            if (txt_btn == "Modify") 
            {
                $('#is_modify').val('1');
                $(".file-caption-main").removeClass("hidden");
                $('#text_btn').text("Revert");
            }
            else 
            {
                $('#is_modify').val('0');
                $(".file-caption-main").addClass("hidden");
                $('#text_btn').text("Modify");
            }
        });
    } 
});

if(page != 'index')
{
    var show_file   = $('#show_file').val();
    //var file_config = $('#file_config').val();
    var file_config = JSON.parse($('#file_config').val());

    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
    '  <div class="modal-content">\n' +
    '    <div class="modal-header">\n' +
    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '      <div class="floating-buttons btn-group"></div>\n' +
    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n';
    
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };
    
    // Icons inside zoom modal classes
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };
    
    // File actions
    var fileActionSettings = {
        showRemove:false,
        showDrag:false,
        showUpload:false,
        showDownload:true,
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        downloadIcon: '<i class="glyphicon glyphicon-download"></i>',
        downloadClass: 'btn btn-kv btn-default btn-outline-secondary',
        downloadTitle: 'Download file',
        //indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        //indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        //indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };
    
    $(".file-input-overwrite").fileinput({
        maxFileCount: 1,
        browseLabel: 'Browse',
        //removeIcon: '<i class="icon-cross3"></i>',
        layoutTemplates: {
            icon: '<i class="icon-file-check"></i>',
            modal: modalTemplate
        },
        initialPreview: [
            show_file
        ],
        initialPreviewConfig: file_config,
        initialPreviewAsData: true,
        overwriteInitial: true,
        deleteUrl: false,
        showUpload: false,
        showClose: false,
        showRemove:true,
        showDownload:true,
        showUpload:false,
        showUploadedThumbs: false,
        previewZoomButtonClasses: previewZoomButtonClasses,
        previewZoomButtonIcons: previewZoomButtonIcons,
        fileActionSettings: fileActionSettings
    });
    
    $('.summernote').summernote({
        height: 300,
        toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
    });
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Data successfully deleted');
        $('#dtTable').DataTable().ajax.reload();
    });
}


