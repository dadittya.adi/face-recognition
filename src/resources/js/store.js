import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        loginState: {
            email: null,
            pass: null
        },
        registerState: {},
        cities: [],
        token: document.head.querySelector('meta[name="csrf-token"]').content
    },
    mutations: {
        UPDATE_CITIES: (state, payload) => {
            state.cities = payload;
        },
    }
});
