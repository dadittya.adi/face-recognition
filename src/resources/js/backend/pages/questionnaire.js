page         = $('#page').val();
$(document).ready( function ()
{ 
    var message         = $('#message').val();

    if (message == 'success') $("#alert_success").trigger("click", 'Data successfully saved');
    else if (message == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated');

    var table = $('#dtTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:25,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/backend/questionnaire/data',
            data: function(d) {
                return $.extend({}, d, {
                    "dimension_id"         : $('#filter_dimension').val(),
                    "indicator_id"         : $('#filter_indicator').val(),
                });
           }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'dimension_name', name: 'dimension',searchable:true,orderable:true},
            {data: 'indicator_name', name: 'indicator_name',searchable:true,orderable:true},
            {data: 'name', name: 'name',searchable:true,orderable:true},
            {data: 'acronym', name: 'acronym',searchable:true,orderable:true},
            {data: 'pola_tc', name: 'pola_tc',searchable:true,orderable:true},
            {data: 'is_active', name: 'is_active',searchable:false,orderable:true},
            {data: 'action', name: 'action',searchable:true,orderable:true},
        ]
    });

    var dtTable = $('#dtTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtTable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtTable.search("").draw();
            }
            return;
    });
    dtTable.draw();

    if(page == 'import')
    {
        list_item = JSON.parse($('#items').val());
        render(); 
    }else
    {
        $('#form').submit(function (event)
        {
            event.preventDefault();
            var code            = $('#code').val();
            var name            = $('#name').val();
            var acronym         = $('#acronym').val();
            var pola_tc         = $('#pola_tc').val();
            var dimension       = $('#select_dimension').val();
            var indicator       = $('#select_indicator').val();
            var is_active       = $('#select_is_active').val();
           
            if(!dimension)
            {
                $("#alert_warning").trigger("click", 'Please select dimension first');
                return false
            }

            if(!indicator)
            {
                $("#alert_warning").trigger("click", 'Please select indicator first');
                return false
            }

            if(!code)
            {
                $("#alert_warning").trigger("click", 'Please type code first');
                return false
            }

            if(!name)
            {
                $("#alert_warning").trigger("click", 'Please type name first');
                return false
            }

            if(!acronym)
            {
                $("#alert_warning").trigger("click", 'Please type acronym first');
                return false
            }

            if(!pola_tc)
            {
                $("#alert_warning").trigger("click", 'Please type pola tc first');
                return false
            }
    
            if(!is_active)
            {
                $("#alert_warning").trigger("click", 'Please select is active first');
                return false
            }
            
            $('#formModal').modal('toggle');
            bootbox.confirm("Are you sure want to save this data ?.", function (result) {
                if(result)
                {
                    $.ajax({
                        type: "POST",
                        url: $('#form').attr('action'),
                        data: $('#form').serialize(),
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function () 
                        {
                            setToDefault();
                            $("#alert_success").trigger("click", 'Data successfully saved');
                            $('#dtTable').DataTable().ajax.reload();
                        },
                        error: function (response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                            $('#formModal').modal();
                        }
                    });
                }
            });
        });
    }
});

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});

$('#form_upload_file').on('change', function () 
{
    //$('#form_upload_file').submit();
    $.ajax({
        type: "post",
        url: $('#form_upload_file').attr('action'),
        data: new FormData(document.getElementById("form_upload_file")),
        processData: false,
        contentType: false,
        beforeSend: function () 
        {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

        },
        complete: function () 
        {
            $.unblockUI();
        },
        success: function (response) 
        {
            $('#form_upload_file').trigger('reset');
            list_item = response;
            render();
        },
        error: function () 
        {
            $.unblockUI();
            $('#form_upload_file').trigger('reset');
        }
    });

})

$('#filter_dimension').select2({
    placeholder: 'Search keyword..',
    ajax: {
        url: '/backend/questionnaire/data-dimension',
        dataType: 'json',
        processResults: function (data) {
        return {
            results:  $.map(data, function (item) {
            return {
                text: item.name,
                id: item.id
            }
            })
        };
        },
        cache: true
    }
});

$('#filter_indicator').select2({
    placeholder: 'Search keyword..',
    ajax: {
        url             : '/backend/questionnaire/data-indicator',
        dataType        : 'json',
        data: function (params) {
            var query = {
                search          : params.term,
                dimension_id    : $('#filter_dimension').val(),
                type            : 'public'
            }
    
            return query;
        },
        processResults: function (data) {
        return {
            results:  $.map(data, function (item) {
            return {
                text: item.name,
                id: item.id
            }
            })
        };
        },
        cache: true
    }
});

function render() 
{
    getIndex();
    $('#items').val(JSON.stringify(list_item));
    var tmpl = $('#questionnaire_table').html();
    Mustache.parse(tmpl);
    var data = { item: list_item };
    console.log(data);
    var html = Mustache.render(tmpl, data);
    $('#questionnaire_tbody').html(html);
   
}

function getIndex() 
{
    for (id in list_item) 
    {
        list_item[id]['_id']  = id;
        list_item[id]['no']   = parseInt(id) + 1;
    }
}

function edit(url)
{
    $.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
        success: function () 
        {
			$.unblockUI();
		},
	})
    .done(function (data) 
    {
        $('#title').text('Update Questionnare');
        $('#code').val(data.code);
        $('#name').val(data.name);
        $('#acronym').val(data.acronym);
        $('#pola_tc').val(data.pola_tc);
        $('#select_dimension').val(data.dimension_id).trigger('change');
        $('#select_indicator').val(data.indicator_id).trigger('change');
        $('#select_is_active').val(data.is_active).trigger('change');
        $('#form').attr('action', data.url_update);
		$('#formModal').modal();
	});
}


function setToDefault()
{
    $('#title').text('Update Questionnare');
    $('#form').attr('action', '#');
    $('#code').val('');
    $('#name').val('');
    $('#acronym').val('');
    $('#pola_tc').val('');
    $('#select_dimension').val('').trigger('change');
    $('#select_indicator').val('').trigger('change');
    $('#select_is_active').val('').trigger('change');
    $("#formModal .close").click()
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Data successfully deleted');
        $('#dtTable').DataTable().ajax.reload();
    });
}
