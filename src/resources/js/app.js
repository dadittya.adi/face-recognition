
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { default: Axios } = require('axios');

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('result-test', require('./components/ResultTest.vue').default);

 const app = new Vue({
     el: '#app',
     data: {
        showUrlImage: false,
        isUploadingImage: false
     },
     methods: {
        uploadImage(event) {
            let url = '/detect-emotion';
            let formData = new FormData();

            formData.append('image', event.target.files[0]);
            this.isUploadingImage = true;

            axios.post(url,formData).then(res => {
                let d = new Date();
                let data = '';
                let teacherwellbeing = 'teacherwellbeing_';
                let currentData = sessionStorage.getItem('teacherwellbeing_');

                if (!currentData) {
                    data = JSON.stringify(res.data);
                } else {
                    newData = JSON.parse(currentData);
                    countData = Object.keys(newData.box).length;

                    newData.box[countData] = res.data.box[0];
                    newData.emotions[countData] = res.data.emotions[0];
                    newData.images[countData] = res.data.images[0];

                    data = JSON.stringify(newData);
                }

                sessionStorage.setItem(teacherwellbeing, data);
                location.href = url;
            })
        },
     }
 });
