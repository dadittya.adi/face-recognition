@extends('layouts.app')

@section('content')
<div class="container py-5">
    <div class="d-flex justify-content-center align-items-center" style="min-height:70vh">
        <div class="card shadow-sm rounded-lg">
            <div class="card-body">
                <h4 class="font-weight-bold text-center mb-3">Masuk</h4>
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email" class="text-sm font-weight-bold mb-0">Email Anda</label>
                        <input type="text" class="form-control" name="email">
                        @if( $errors->has('email') )
                        <small class="form-text text-danger">
                            {{ $errors->first('email') }}
                        </small>
                        @else
                        <p class="form-text text-muted text-sm">Contoh: emailanda@gmail.com</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-sm font-weight-bold">Kata Sandi</label>
                        <div class="input-group append">
                            <input type="password" class="form-control" name="password" id="password" ref="password">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-input-group text-sm" @click.prevent="showPasswd()"><i :class="this.isPasswdShow ? 'fas fa-eye' : 'fas fa-eye-slash' "></i></button>
                            </div>
                        </div>
                        @if( $errors->has('password') )
                        <small class="form-text text-danger">
                            {{ $errors->first('password') }}
                        </small>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="text-sm d-flex align-items-center">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="1" id="remember">
                                <label for="remember" class="form-check-label text-muted">Ingat Saya</label>
                            </div>
                            <a href="{{ route('password.request') }}" class="ml-auto">Ubah kata sandi ?</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary my-4">Masuk</button>
                    </div>
                </form>
                <p class="text-sm text-muted mb-0">
                    Belum punya akun Teacher Wellbeing? <a href="{{ route('register') }}">Daftar</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
