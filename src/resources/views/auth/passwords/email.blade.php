@extends('layouts.app')

@section('content')
<div class="container py-5">
    <div class="d-flex justify-content-center align-items-center" style="min-height:70vh">
        <div class="card shadow-sm rounded-lg">
            <div class="card-body">
                <h4 class="font-weight-bold text-center mb-3">Reset Password</h4>
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group">
                        <label for="email" class="text-sm font-weight-bold mb-0">Email Anda</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
                <p class="text-sm text-muted mb-0">
                    Belum punya akun Teacher Wellbeing? <a href="{{ route('register') }}">Daftar</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection


