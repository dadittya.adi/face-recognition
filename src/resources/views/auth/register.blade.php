@extends('layouts.app')

@section('content')
<div class="container py-5">
    <div class="d-flex justify-content-center align-items-center" style="min-height:70vh">
        <div class="card shadow-sm rounded-lg">
            <div class="card-body">
                <h4 class="font-weight-bold text-center mb-3">Daftar</h4>
                <form action="{{ route('register') }}" id="form-register" method="post" autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label for="name" class="text-sm font-weight-bold mb-0">Nama</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                        @if( $errors->has('name') )
                        <small class="form-text text-danger">
                            {{ $errors->first('name') }}
                        </small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email" class="text-sm font-weight-bold mb-0">Email</label>
                        <input type="text" class="form-control" name="email" id="email" value="{{ old('email') }}">
                        @if( $errors->has('email') )
                        <small class="form-text text-danger">
                            {{ $errors->first('email') }}
                        </small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-sm font-weight-bold mb-0">Password</label>
                        <input type="password" class="form-control" name="password" id="password">
                        @if( $errors->has('password') )
                        <small class="form-text text-danger">
                            {{ $errors->first('password') }}
                        </small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="text-sm font-weight-bold mb-0">Konfirmasi Password</label>
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary my-4" ref="verbtn">Daftar</button>
                    </div>
                </form>
                <p class="text-sm text-muted mb-0">
                    Sudah punya akun Teacher Wellbeing? <a href="/login">Masuk</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
