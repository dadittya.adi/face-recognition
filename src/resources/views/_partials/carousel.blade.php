<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="carousel-caption text-left">
                            <h2 class="display-3 font-weight-bold">Lorem Ipsum per sempre</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mollis mauris nec diam tempus</p>
                            <button class="btn btn-light btn-block rounded-pill px-3 d-flex align-items-center" data-toggle="modal" data-target="#selectImage"><i class="fas fa-image mr-2"></i> Upload Foto Anda <i class="fas fa-search ml-auto"></i></button>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <img src="{{ asset('images/banner.png') }}" class="img-fluid" alt="...">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
