@foreach( App\Models\Articles::where('is_active',true)->orderBy('id','desc')->paginate(3) as $article )
<div class="col-md-4">
    <div class="post-thumbnail">
        <img src="{{ route('landing.image.show',['article', $article->image]) }}" class="img-fluid" alt="">
        <h4 class="post-title my-3"><a href="{{ route('landing.artikel.single', $article->slug) }}">{{ $article->title }}</a></h4>
        <div class="text-sm text-muted mb-3">Posted on {{ date("M d, Y", strtotime($article->created_at)) }}</div>
        <p>
            {{ \Illuminate\Support\Str::limit(strip_tags($article->content),200) }} <a href="{{ route('landing.artikel.single', $article->slug) }}" class="text-sm">Read More  ></a>
        </p>
    </div>
</div>
@endforeach
