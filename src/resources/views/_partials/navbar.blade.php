<nav class="navbar navbar-expand-md navbar-light navbar-menu">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item px-md-3 {{ request()->routeIs('landing.home') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('landing.home') }}">Beranda</a>
                </li>
                <li class="nav-item px-md-3 {{ request()->routeIs('landing.hki') || request()->routeIs('landing.profile') || request()->routeIs('landing.faq') ? 'active' : '' }} dropdown">
                    <a id="infoMenu" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Informasi
                    </a>

                    <div class="dropdown-menu" aria-labelledby="infoMenu">
                        <a class="dropdown-item" href="{{ route('landing.hki') }}">Informasi HKI Terkait Test</a>
                        <a class="dropdown-item" href="{{ route('landing.profile') }}">Profile Peneliti</a>
                        <a class="dropdown-item" href="{{ route('landing.faq') }}">FAQ</a>
                    </div>
                </li>
                <li class="nav-item px-md-3 {{ request()->routeIs('landing.artikel') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('landing.artikel') }}">Artikel</a>
                </li>
                <li class="nav-item px-md-3 {{ request()->routeIs('landing.publikasi') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('landing.publikasi') }}">Publikasi</a>
                </li>
                <li class="nav-item px-md-3 {{ request()->routeIs('landing.gallery') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('landing.gallery') }}">Gallery</a>
                </li>
                <li class="nav-item px-md-3 {{ request()->routeIs('landing.about') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('landing.about') }}">Tentang Kami</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
