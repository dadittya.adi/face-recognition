<section class="bg-purple">
    <div class="container bg-ornamen py-5">
        <div class="position-relative">
            <nav class="breadcrumb bg-transparent p-0 text-sm position-md-absolute">
                @foreach($links as $link)
                <a class="breadcrumb-item text-white" href="{{ $link['url'] }}">{{ $link['name'] }}</a>
                @endforeach
                <span class="breadcrumb-item text-white active">{{ $page_title }}</span>
            </nav>
            <h1 class="font-weight-bold text-white text-lg">{{ $page_title }}</h1>
        </div>
    </div>
</section>
