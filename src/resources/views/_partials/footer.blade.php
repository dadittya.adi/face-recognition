<section class="bg-light">
    <div class="container">
        <div class="row py-5">
            <div class="col-md-5">
                <h3 class="font-weight-bold text-primary">Teacher <br> Wellbeing</h3>
                <p class="text-sm py-2">Jl. Pahlawan No.9, Mugassari, Kec. Semarang Sel. Kota Semarang, Jawa Tengah 50249</p>
                <div class="d-flex mx-n3 mb-4">
                    <div class="px-3">
                        <h6 class="text-sm text-uppercase">Kontak</h6>
                        <p class="text-sm">024 - 572 572</p>
                    </div>
                    <div class="px-3">
                        <h6 class="text-sm text-uppercase">Email</h6>
                        <p class="text-sm">bantuan@kompetensiguru.com</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5 px-md-5">
                <div class="d-flex mx-n4 mb-4 text-sm">
                    <ul class="list-unstyled mx-4">
                        <li class="py-1"><a href="#" class="text-body">Beranda</a></li>
                        <li class="py-1"><a href="#" class="text-body">Informasi</a></li>
                        <li class="py-1"><a href="#" class="text-body">Artikel</a></li>
                    </ul>
                    <ul class="list-unstyled mx-4">
                        <li class="py-1"><a href="#" class="text-body">Publikasi</a></li>
                        <li class="py-1"><a href="#" class="text-body">Gallery</a></li>
                        <li class="py-1"><a href="#" class="text-body">Tentang Kami</a></li>
                    </ul>
                </div>
                <div class="socmed">
                    <h6>FOLLOW</h6>
                    <ul class="list-unstyled d-flex mx-n3">
                        <li class="px-3"><a href="#" class="text-lg"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="px-3"><a href="#" class="text-lg"><i class="fab fa-youtube"></i></a></li>
                        <li class="px-3"><a href="#" class="text-lg"><i class="fab fa-instagram"></i></a></li>
                        <li class="px-3"><a href="#" class="text-lg"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <p class="text-center mb-0 pb-3 text-sm">© 2020 Teacher Wellbeing. All rights reserved.</p>
        <a href="#" class="scroll-top"><i class="fas fa-chevron-up text-lg"></i></a>
    </div>
</section>
