<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if(isset($edit))
                <li><a href="#" onclick="edit('{!! $edit !!}')"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if(isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-trash"></i> Delete</a></li>
            @endif
        </ul>
        
    </li>
</ul>