@extends('backend.layouts.app', ['active' => 'publication'])

@section('page-css')
    <link rel="stylesheet" href="{{ asset('vendor/summernote/summernote.css') }}">
@endsection

@section('page-header')
	<div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Publication</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('backend.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('backend.publication.index') }}">Publication</a></li>
                <li class="active">Create</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('backend.publication.store'),
                'method'    => 'POST',
                'id'        => 'form',
                'enctype'   => 'multipart/form-data'
            ])
        !!}

        
            @include('backend.form.date', [
                'field'             => 'published_date',
                'label'             => 'Published Date',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'mandatory' 		=> '*Required',
                'class'             => 'daterange-single',
                'placeholder'       => 'dd/mm/yyyy',
                'attributes'        => [
                    'id'            => 'published_date',
                    'readonly'      => 'readonly'
                ]
            ])
                    
            @include('backend.form.text', [
                'field' 			=> 'title',
                'label' 			=> 'Title',
                'mandatory' 		=> '*Required',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'title',
                    'autocomplete' 	=> 'off'
                ]
            ])

            @include('backend.form.textarea', [
                'field' 			=> 'abstract',
                'label' 			=> 'Abstract',
                'mandatory' 		=> '*Required',
                'class' 			=> 'summernote',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'abstract',
                    'autocomplete' 	=> 'off'
                ]
            ])

            @include('backend.form.file', [
                'field' 			=> 'file',
                'label' 			=> 'File',
                'class' 			=> 'file-input-overwrite',
                'mandatory' 		=> '*Required',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'file',
                    'autocomplete' 	=> 'off'
                ]
            ])

            @include('backend.form.checkbox', [
				'field' 		=> 'is_active',
				'label' 		=> 'Is Active',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'style_checkbox' => 'checkbox checkbox-switchery',
				'class' 		=> 'switchery',
				'attributes' 	=> [
					'id' 		=> 'checkbox_is_active'
				]
            ])
            
            <button type="submit" class="btn btn-primary col-xs-12 btn-lg" >Save <i class="icon-floppy-disk position-right"></i></button>
        {!! Form::close() !!}
        </div>
    </div>

    {!! Form::hidden('is_modify', '1', array('id' => 'is_modify')) !!}
    {!! Form::hidden('page', 'create', array('id' => 'page')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/switch.js') }}"></script>
    <script src="{{ mix('js/fileinput.js') }}"></script>
    <script src="{{ mix('js/summernote.js') }}"></script>
    <script src="{{ mix('js/publication.js') }}"></script>
@endsection