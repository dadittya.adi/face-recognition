@extends('backend.layouts.app', ['active' => 'gallery'])

@section('page-header')
	<div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Gallery</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('backend.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('backend.gallery.index') }}">Gallery</a></li>
                <li class="active">Create</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('backend.gallery.store'),
                'method'    => 'POST',
                'id'        => 'form',
                'enctype'   => 'multipart/form-data'
            ])
        !!}

            @include('backend.form.select', [
                'field'         => 'album',
                'label'         => 'Album',
                'mandatory'     => '*Required',
                'options'       => [
                    ''          => '-- Select Album --',
                ]+$albums,
                'class'         => 'select-search',
                'label_col' 	=> 'col-xs-12',
                'form_col' 		=> 'col-xs-12',
                'attributes'    => [
                    'id'        => 'select_album'
                ]
            ])
            
            @include('backend.form.textarea', [
                'field' 			=> 'description',
                'label' 			=> 'Description',
                'mandatory' 		=> '*Required',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'description',
                    'autocomplete' 	=> 'off'
                ]
            ])

            @include('backend.form.file', [
                'field' 			=> 'image[]',
                'label' 			=> 'Image',
                'class' 			=> 'file-input-overwrite',
                'mandatory' 		=> '*Required',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'image',
                    'autocomplete' 	=> 'off',	    
                    'multiple' 	    => 'multiple'	    
                ]
            ])

            <button type="submit" class="btn btn-primary col-xs-12 btn-lg" >Save <i class="icon-floppy-disk position-right"></i></button>
        {!! Form::close() !!}
        </div>
    </div>

    {!! Form::hidden('is_modify', '1', array('id' => 'is_modify')) !!}
    {!! Form::hidden('page', 'create', array('id' => 'page')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/fileinput.js') }}"></script>
    <script src="{{ mix('js/gallery.js') }}"></script>
@endsection