@extends('backend.layouts.app', ['active' => 'additional_task'])

@section('page-header')
	<div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Additional Task</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('backend.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Additional Task</li>
            </ul>
            <ul class="breadcrumb-elements">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
                        <i class="icon-three-bars position-left"></i>
                        Actions
                        <span class="caret"></span>
                    </a>
                    
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" data-toggle="modal" data-target="#formModal"><i class="icon-plus2 pull-right"></i> Create</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
	<div class="panel panel-flat">
			<div class="panel-body">
				<div class="table-responsive">
						<table class="table table-basic table-striped table-hover" id="dtTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Name</th>
									<th>Status Active</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
				</div>
			</div>
		</div>

        
        {!! Form::hidden('url_insert',route('backend.additionalTask.store'), array('id' => 'url_insert')) !!}
@endsection

@section('page-modal')
    @include('backend.additional_task._form_modal')
@endsection


@section('page-js')
    <script src="{{ mix('js/additional_task.js') }}"></script>
@endsection