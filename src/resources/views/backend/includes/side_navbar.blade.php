<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                    <h6>{{ Auth::guard('admins')->user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::guard('admins')->user()->email }}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My Account</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('backend.accountSetting') }}"><i class="icon-cog5"></i> <span>Account Setting</span></a></li>
                    <li><a href="#"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('backend.doLogout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'admin' ? 'active' : '' }}"><a href="{{ route('backend.admin.index') }}"><i class="icon-file-text"></i> <span>Admin</span></a></li>
                    <li class="{{ $active == 'album' ? 'active' : '' }}"><a href="{{ route('backend.album.index') }}"><i class="icon-file-text"></i> <span>Album</span></a></li>
                    <li class="{{ $active == 'gallery' ? 'active' : '' }}"><a href="{{ route('backend.gallery.index') }}"><i class="icon-file-text"></i> <span>Gallery</span></a></li>
                    <li class="{{ $active == 'article' ? 'active' : '' }}"><a href="{{ route('backend.article.index') }}"><i class="icon-file-text"></i> <span>Article</span></a></li>
                    <li class="{{ $active == 'researcher' ? 'active' : '' }}"><a href="{{ route('backend.researcher.index') }}"><i class="icon-file-text"></i> <span>Researcher</span></a></li>
                    <li class="{{ $active == 'publication' ? 'active' : '' }}"><a href="{{ route('backend.publication.index') }}"><i class="icon-file-text"></i> <span>Publication</span></a></li>
                    <li class="{{ $active == 'test_answer' ? 'active' : '' }}"><a href="{{ route('backend.testResult.index') }}"><i class="icon-archive"></i> <span>Test Answer</span></a></li>
                    <!-- li class="{{ $active == 'age_category' ? 'active' : '' }}"><a href="{{ route('backend.ageCategory.index') }}"><i class=" icon-file-text"></i> <span>Age Category</span></a></li>
                    <li class="{{ $active == 'education' ? 'active' : '' }}"><a href="{{ route('backend.education.index') }}"><i class=" icon-file-text"></i> <span>Education</span></a></li>
                    <li class="{{ $active == 'employement_status' ? 'active' : '' }}"><a href="{{ route('backend.employementStatus.index') }}"><i class=" icon-file-text"></i> <span>Employement Status</span></a></li>
                    <li class="{{ $active == 'school_level' ? 'active' : '' }}"><a href="{{ route('backend.schoolLevel.index') }}"><i class=" icon-file-text"></i> <span>School Level</span></a></li>
                    <li class="{{ $active == 'teacher_type' ? 'active' : '' }}"><a href="{{ route('backend.teacherType.index') }}"><i class=" icon-file-text"></i> <span>Teacher Type</span></a></li>
                    <li class="{{ $active == 'subject' ? 'active' : '' }}"><a href="{{ route('backend.subject.index') }}"><i class=" icon-file-text"></i> <span>Subject</span></a></li>
                    <li class="{{ $active == 'year_experience' ? 'active' : '' }}"><a href="{{ route('backend.yearExperience.index') }}"><i class=" icon-file-text"></i> <span>Year Experience</span></a></li>
                    <li class="{{ $active == 'year_experience' ? 'active' : '' }}"><a href="{{ route('backend.additionalTask.index') }}"><i class=" icon-file-text"></i> <span>Additional Task</span></a></li>
                    <li class="{{ $active == 'year_experience' ? 'active' : '' }}"><a href="{{ route('backend.dimension.index') }}"><i class=" icon-file-text"></i> <span>Dimension</span></a></li-->
                   
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
