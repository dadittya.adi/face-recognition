@extends('backend.layouts.app', ['active' => 'researcher'])

@section('page-css')
    <link rel="stylesheet" href="{{ asset('vendor/summernote/summernote.css') }}">
@endsection

@section('page-header')
	<div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Researcher</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('backend.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('backend.researcher.index') }}">Researcher</a></li>
                <li class="active">Edit</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('backend.researcher.update',$data->id),
                'method'    => 'POST',
                'id'        => 'form',
                'enctype'   => 'multipart/form-data'
            ])
        !!}

        
            @include('backend.form.text', [
                'field' 			=> 'name',
                'label' 			=> 'Name',
                'default' 			=> $data->name,
                'mandatory' 		=> '*Required',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'name',
                    'autocomplete' 	=> 'off'
                ]
            ])

            @include('backend.form.textarea', [
                'field' 			=> 'content',
                'label' 			=> 'Content',
                'default' 			=> $data->content,
                'mandatory' 		=> '*Required',
                'class' 			=> 'summernote',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'content',
                    'autocomplete' 	=> 'off'
                ]
            ])

            @include('backend.form.file', [
                'field' 			=> 'image',
                'label' 			=> 'Image',
                'default' 			=> $data->image,
                'class' 			=> 'file-input-overwrite',
                'mandatory' 		=> '*Required',
                'label_col' 		=> 'col-xs-12',
                'form_col' 			=> 'col-xs-12',
                'attributes' 		=> [
                    'id' 			=> 'image',
                    'autocomplete' 	=> 'off'
                ]
            ])
            <button type="button" class="btn col-md-6 col-md-offset-6 btn-lg" id="btn_modify"><span id="text_btn">Modify</span> <i class="icon-images2 position-right"></i></button>
            

            @include('backend.form.checkbox', [
				'field' 		=> 'is_active',
				'label' 		=> 'Is Active',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'style_checkbox' => 'checkbox checkbox-switchery',
				'class' 		=> 'switchery',
				'attributes' 	=> [
					'id' 		=> 'checkbox_is_active'
				]
            ])

            {!! Form::hidden('is_modify', '0', array('id' => 'is_modify')) !!}
            <button type="submit" class="btn btn-primary col-xs-12 btn-lg" >Save <i class="icon-floppy-disk position-right"></i></button>
        {!! Form::close() !!}
        </div>
    </div>

    {!! Form::hidden('show_image', route('backend.researcher.showImage',$data->image), array('id' => 'show_image')) !!}
    {!! Form::hidden('is_active', $data->is_active, array('id' => 'is_active')) !!}
    {!! Form::hidden('page', 'edit', array('id' => 'page')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/switch.js') }}"></script>
    <script src="{{ mix('js/fileinput.js') }}"></script>
    <script src="{{ mix('js/summernote.js') }}"></script>
    <script src="{{ mix('js/researcher.js') }}"></script>
@endsection