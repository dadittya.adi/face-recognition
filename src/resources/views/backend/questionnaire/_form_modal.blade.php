<div id="formModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> '#',
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form'
					])
				!!}
				<div class="modal-header bg-indigo">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title"><span id="title">Edit Questionnaire</span></h5>
				</div>
				<div class="modal-body">
					@include('backend.form.select', [
						'field'         => 'dimension',
						'label'         => 'Dimension',
						'mandatory'     => '*Required',
						'options'       => $dimensions,
						'class'         => 'select-search',
						'label_col' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'attributes'    => [
							'id'        => 'select_dimension'
						]
					])

					@include('backend.form.select', [
						'field'         => 'indicator',
						'label'         => 'Indicator',
						'mandatory'     => '*Required',
						'options'       => $indicators,
						'class'         => 'select-search',
						'label_col' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'attributes'    => [
							'id'        => 'select_indicator'
						]
					])

					@include('backend.form.text', [
						'field' 			=> 'code',
						'label' 			=> 'Code',
						'mandatory' 		=> '*Required',
						'label_col' 		=> 'col-xs-12',
						'form_col' 			=> 'col-xs-12',
						'attributes' 		=> [
							'id' 			=> 'code',
							'autocomplete' 	=> 'off'
						]
					])

					@include('backend.form.text', [
						'field' 			=> 'name',
						'label' 			=> 'Name',
						'mandatory' 		=> '*Required',
						'label_col' 		=> 'col-xs-12',
						'form_col' 			=> 'col-xs-12',
						'attributes' 		=> [
							'id' 			=> 'name',
							'autocomplete' 	=> 'off'
						]
					])

					@include('backend.form.text', [
						'field' 			=> 'acronym',
						'label' 			=> 'Acronym',
						'mandatory' 		=> '*Required',
						'label_col' 		=> 'col-xs-12',
						'form_col' 			=> 'col-xs-12',
						'attributes' 		=> [
							'id' 			=> 'acronym',
							'autocomplete' 	=> 'off'
						]
					])


					@include('backend.form.text', [
						'field' 			=> 'pola_tc',
						'label' 			=> 'Pola TC',
						'mandatory' 		=> '*Required',
						'label_col' 		=> 'col-xs-12',
						'form_col' 			=> 'col-xs-12',
						'attributes' 		=> [
							'id' 			=> 'pola_tc',
							'autocomplete' 	=> 'off'
						]
					])

					@include('backend.form.select', [
						'field'         => 'is_active',
						'label'         => 'Status',
						'mandatory'     => '*Required',
						'options'       => [
							''          => '-- Select Status --',
							'active'     => 'Active',
							'not-active' => 'Not Active'
						],
						'class'         => 'select-search',
						'label_col' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'attributes'    => [
							'id'        => 'select_is_active'
						]
					])
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" onClick="setToDefault()">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
