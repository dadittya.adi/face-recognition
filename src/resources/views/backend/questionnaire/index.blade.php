@extends('backend.layouts.app', ['active' => 'questionnaire'])

@section('page-header')
	<div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Questionnaire</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('backend.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Questionnaire</li>
            </ul>
            <ul class="breadcrumb-elements">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
                        <i class="icon-three-bars position-left"></i>
                        Actions
                        <span class="caret"></span>
                    </a>
                    
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('backend.questionnaire.import') }}"><i class="icon-plus2 pull-right"></i> Import</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                @include('backend.form.select', [
                    'field'         => 'filter_dimension',
                    'label'         => 'Filter Dimension',
                    'options'       => [
                        ''          => '-- Select Filter Dimension --',
                    ]+$dimensions,
                    'class'         => 'select-search',
                    'label_col' 	=> 'col-xs-12',
                    'form_col' 		=> 'col-xs-12',
                    'attributes'    => [
                        'id'        => 'filter_dimension'
                    ]
                ])

                @include('backend.form.select', [
                    'field'         => 'filter_indicator',
                    'label'         => 'Filter Indicator',
                    'options'       => [
                        ''          => '-- Select Filter Indicator --',
                    ]+$indicators,
                    'class'         => 'select-search',
                    'label_col' 	=> 'col-xs-12',
                    'form_col' 		=> 'col-xs-12',
                    'attributes'    => [
                        'id'        => 'filter_indicator'
                    ]
                ])
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-basic table-striped table-hover" id="dtTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Dimension</th>
                                <th>Indicator</th>
                                <th>Name</th>
                                <th>Acronym</th>
                                <th>Pola TC</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    {!! Form::hidden('message', $message, array('id' => 'message')) !!}
    {!! Form::hidden('page', 'index', array('id' => 'page')) !!}
@endsection

@section('page-modal')
    @include('backend.questionnaire._form_modal')
@endsection


@section('page-js')
    <script src="{{ mix('js/questionnaire.js') }}"></script>
@endsection