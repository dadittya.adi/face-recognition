@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ asset('css/maps.css') }}">
@endpush
@section('content')
    @include('_partials.newsbar')
    @include('_partials.carousel')
    <section class="bg-robot d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <h3 class="font-weight-bold display-5 mb-3">Bagaimana Teacher Wellbeing dapat membantu anda</h3>
                </div>
                <div class="col-lg-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                    <a href="#" class="btn btn-primary mt-5">Lihat Selengkapnya</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="{{ asset('images/why-tw.png') }}" alt="">
                </div>
                <div class="col-lg-6">
                    <h3 class="font-weight-bold display-5 mb-3">Mengapa harus Teacher Wellbeing?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                    <a href="#" class="btn btn-primary mt-5">Lihat Selengkapnya</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="font-weight-bold text-center mb-5 pb-4">Tujuan Teacher Wellbeing</h2>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('images/male_avatar.png') }}" class="img-fluid mb-5" style="width:150px;height:150px;" alt="">
                            <h4>Tujuan 1</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('images/male_avatar.png') }}" class="img-fluid mb-5" style="width:150px;height:150px;" alt="">
                            <h4>Tujuan 2</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('images/male_avatar.png') }}" class="img-fluid mb-5" style="width:150px;height:150px;" alt="">
                            <h4>Tujuan 3</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-purple">
        <div class="container text-white text-center p-5">
            <h4 class="font-weight-bold">Neque porro quisquam est qui dolorem ipsum quia dolor <br>
            sit amet, consectetur, adipisci velit...</h4>
            <p>with our annual deductible & co-insurance options!</p>
            <button class="btn btn-light px-4 rounded-pill inline-block" data-toggle="modal" data-target="#selectImage">Upload Foto Anda</button>
        </div>
    </section>

    <section>
        <div class="container py-5">
            <h2 class="text-center text-green font-weight-bold mb-2">Artikel Terbaru</h2>
            <p class="text-center mb-5">Artikel, berita dan kegiatan seputar Teacher Wellbeing</p>
            <div class="row">
                @foreach( $articles as $article )
                    <div class="col-md-4">
                        <div class="post-thumbnail">
                            <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                            <h4 class="post-title my-3"><a href="{{ route('landing.artikel.single', $article->slug) }}">{{ $article->title }}</a></h4>
                            <div class="text-sm text-muted mb-3">Posted on {{ date("M d, Y", strtotime($article->created_at)) }}</div>
                            <p>
                                {{ \Illuminate\Support\Str::limit(strip_tags($article->content),100) }}
                                <a href="{{ route('landing.artikel.single', $article->slug) }}" class="text-sm">Read More  ></a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                <a href="{{ route('landing.artikel') }}" class="mt-4 btn btn-primary">Lihat Semua</a>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="{{ asset('js/maps.js') }}"></script>
@endpush
