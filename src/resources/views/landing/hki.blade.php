@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Informasi HKI Tekait Tes',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <h2 class="text-primary font-weight-bold">Tentang  Teacher Wellbeing</h2>
        <p class="mb-5">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae justo libero. Phasellus sed dictum libero, in euismod elit. Sed ut mauris at justo accumsan egestas. Sed vitae dolor non sapien pellentesque lacinia bibendum eu neque. Suspendisse vitae velit eu diam faucibus varius tristique in orci. Nulla eu lectus quis sem dapibus congue. Phasellus in orci felis. Aliquam imperdiet sed ipsum non ullamcorper. Phasellus mollis tellus et sem hendrerit, condimentum tempor elit tincidunt. Vestibulum mollis et urna quis tincidunt. Ut eget erat non nisi molestie tincidunt. Donec justo massa, consequat in auctor a, tincidunt nec dolor. Morbi malesuada iaculis enim at auctor. Donec quis volutpat libero. Proin molestie viverra metus consequat mollis. Vivamus vitae mattis mi.
            Nulla et elementum arcu, et ultrices arcu. Quisque condimentum molestie libero, at interdum massa lobortis ac. Pellentesque sed sodales nibh, eget pulvinar nisl. In felis dolor, molestie nec eros malesuada, iaculis aliquam neque. Aliquam hendrerit ipsum in libero hendrerit aliquet. Fusce eget sem ac erat lacinia pharetra at eget arcu. In vel posuere nibh, vel tempor leo. Nullam massa tortor, mollis eu sem sodales, blandit auctor nibh.
        </p>
        <h2 class="text-primary font-weight-bold">Mengapa perlu Teacher Wellbeing?</h2>
        <p class="mb-5">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae justo libero. Phasellus sed dictum libero, in euismod elit. Sed ut mauris at justo accumsan egestas. Sed vitae dolor non sapien pellentesque lacinia bibendum eu neque. Suspendisse vitae velit eu diam faucibus varius tristique in orci. Nulla eu lectus quis sem dapibus congue. Phasellus in orci felis. Aliquam imperdiet sed ipsum non ullamcorper. Phasellus mollis tellus et sem hendrerit, condimentum tempor elit tincidunt. Vestibulum mollis et urna quis tincidunt. Ut eget erat non nisi molestie tincidunt. Donec justo massa, consequat in auctor a, tincidunt nec dolor. Morbi malesuada iaculis enim at auctor. Donec quis volutpat libero. Proin molestie viverra metus consequat mollis. Vivamus vitae mattis mi.
            Nulla et elementum arcu, et ultrices arcu. Quisque condimentum molestie libero, at interdum massa lobortis ac. Pellentesque sed sodales nibh, eget pulvinar nisl. In felis dolor, molestie nec eros malesuada, iaculis aliquam neque. Aliquam hendrerit ipsum in libero hendrerit aliquet. Fusce eget sem ac erat lacinia pharetra at eget arcu. In vel posuere nibh, vel tempor leo. Nullam massa tortor, mollis eu sem sodales, blandit auctor nibh.
        </p>
        <button class="mt-4 d-flex mx-auto btn btn-primary">Load More</button>
    </div>
</section>
@endsection
