@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Gallery',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <div class="row">
            @foreach($galleries as $album)
            <div class="col-md-4">
                <div class="post-gallery">
                    <a href="#">
                        <img src="{{ route('landing.image.show', ['gallery', $album->gallery->first()->image] ) }}" class="img-fluid" alt="">
                        <h4 class="post-title my-3">{{ $album->title }}</h4>
                    </a>
                    <div class="text-sm mb-3 mx-n1">
                        <span class="px-1">{{ date('d M Y', strtotime($album->created_at)) }}</span>
                        <span class="px-1">|</span>
                        <span class="px-1" style="color: var(--orange)">[{{ $album->gallery->count() }} Foto]</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="mt-4 d-flex justify-content-center">
            {{ $galleries->links() }}
        </div>
    </div>
</section>
@endsection
