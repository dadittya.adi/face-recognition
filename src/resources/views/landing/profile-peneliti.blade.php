@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Profile Peneliti',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <div class="list-figured">
            <div class="figured-item figured-left">
                <div class="figured-image">
                    <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="figured-desc">
                    <h2 class="font-weight-bold text-green mb-4">Prof. Robert Dedjumadilakir S.E., M.A., - Ph.D</h2>
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                </div>
            </div>
        </div>

        <h3 class="text-green text-md font-weight-bold">Research Interests</h3>
        <p>Development Economics, Urban Economics, Econometrics, Industrial Organization</p>
        <hr class="my-5">
        <h3 class="text-green text-md font-weight-bold">Riwayat Publikasi</h3>
        <p>
            A. Buku
            Dedjumadilakir, R.   Loremipsum Dolor Sit Per Sempre. Forda Press (Anggota IKAPI).  Bogor. ISBN : 978-602-71770-4-8
            B. Karya tulis ilmiah terbit dalam jurnal ilmiah nasional terakreditasi
            Calabrese, Michael. “Between Despair and Ecstacy: Marco Polo’s Life of the Buddha.” Exemplaria 9.1 (1997). 22 June 1998
            United States. Dept. of Justice. Natl. Inst. Of Justice. Prosecuting Gangs: A National Assessment. By Claire Johnson, Barbara Webster, and Edward Connors. Feb 1996. 29 June 1998.
            Angier, Natalie “Chemists Learn Why Vegetables are Good for You.” New York Times 13 Apr. 1993, late ed.: C1. New York Times On disc. CD-ROM. UMIProquest. Oct. 1993.
        </p>
        <hr class="my-5">
        <h3 class="text-green text-md font-weight-bold">Social Media</h3>
        <div class="share-menu">
            <a href="#" class="share-btn wa"><i class="fab fa-whatsapp"></i></a>
            <a href="#" class="share-btn fb"><i class="fab fa-facebook-f"></i></a>
            <a href="#" class="share-btn tw"><i class="fab fa-twitter"></i></a>
            <a href="#" class="share-btn sh"><i class="fas fa-share-alt"></i></a>
        </div>
    </div>
</section>
@endsection
