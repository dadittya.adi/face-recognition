@extends('layouts.app')

@push('meta')
<meta property="og:url" content="{{ url()->current() }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ \Illuminate\Support\Str::title($article->title) }}" />
<meta property="og:description" content="{{ \Illuminate\Support\Str::limit(strip_tags($article->content),100) }}" />
<meta property="og:image" content="{{ route('landing.image.show',['article', $article->image]) }}" />
@endpush

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => \Illuminate\Support\Str::title($article->title),
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
        [
            'name' => 'Artikel',
            'url' => route('landing.artikel'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <article class="mb-5">
            <div class="article-title">
                <h1 class="font-weight-bold">{{ \Illuminate\Support\Str::title($article->title) }}</h1>
                <div class="text-sm text-muted mb-3">Posted on {{ date("M d, Y", strtotime($article->created_at)) }}</div>
            </div>
            <div class="article-banner">
                <img src="{{ route('landing.image.show',['article', $article->image]) }}" class="img-fluid" alt="">
            </div>
            <div class="article-content bg-light">
                <p>{!! $article->content !!}</p>
                <hr class="mt-5">
                <div class="d-flex justify-content-end align-items-center">
                    Bagikan Artikel
                    <div class="ml-3 share-menu">
                        <a href="https://api.whatsapp.com/send?text={{ url()->current() }}" class="share-btn wa" target="_blank"><i class="fab fa-whatsapp"></i></a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="share-btn fb" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="http://twitter.com/share?url={{ url()->current() }}" class="share-btn tw" target="_blank"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </article>
        <div class="row">
            <h3 class="col-md-12 my-5 font-weight-bold text-green">Artikel Terbaru</h3>
            @include('_partials.newest-article')
        </div>
    </div>
</section>
@endsection
