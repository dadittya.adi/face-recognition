@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'FACE DETECT RESULT',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <div class="list-figured">
            <result-test></result-test>
        </div>
    </div>
</section>
@endsection
