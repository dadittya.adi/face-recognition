@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Profile Peneliti',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
        [
            'name' => 'Informasi',
            'url' => route('landing.hki'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <div class="list-figured">
            <div class="figured-item figured-left">
                <div class="figured-image">
                    <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="figured-desc">
                    <h2 class="font-weight-bold text-green mb-4">Apa itu Teacher Wellbeing?</h2>
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                    <a href="#" class="btn btn-primary inline-block">Lihat Selengkapnya</a>
                </div>
            </div>
            <div class="figured-item figured-right">
                <div class="figured-image">
                    <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="figured-desc">
                    <h2 class="font-weight-bold text-green mb-4">Apa itu Teacher Wellbeing?</h2>
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                    <a href="#" class="btn btn-primary inline-block">Lihat Selengkapnya</a>
                </div>
            </div>
            <div class="figured-item figured-left">
                <div class="figured-image">
                    <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="figured-desc">
                    <h2 class="font-weight-bold text-green mb-4">Apa itu Teacher Wellbeing?</h2>
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                    <a href="#" class="btn btn-primary inline-block">Lihat Selengkapnya</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
