@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => \Illuminate\Support\Str::title($publication->title),
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
        [
            'name' => 'Publikasi',
            'url' => route('landing.publikasi'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <article class="mb-5">
            <div class="article-title text-center pb-0">
                <h1 class="font-weight-bold mb-4">{{ \Illuminate\Support\Str::title($publication->title) }}</h1>
                {{--<p class="text-sm text-muted mb-0">Oleh: Fadli</p>--}}
            </div>
            <div class="article-content">
                <p class="font-weight-bold text-center">Abstract</p>
                <p>{!! $publication->abstract !!}</p>

                <div class="text-center mt-5">
                    <a href="{{ route('landing.image.show', ['publication',$publication->file]) }}" class="btn btn-primary" target="_blank">Download as PDF</a>
                </div>
                <hr class="mt-5">
                <div class="d-flex justify-content-end align-items-center">
                    Bagikan Artikel
                    <div class="ml-3 share-menu">
                        <a href="https://api.whatsapp.com/send?text={{ url()->current() }}" class="share-btn wa" target="_blank"><i class="fab fa-whatsapp"></i></a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="share-btn fb" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="http://twitter.com/share?url={{ url()->current() }}" class="share-btn tw" target="_blank"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </article>
        <div class="row">
            <h3 class="col-md-12 my-5 font-weight-bold text-green">Artikel Terbaru</h3>
            @include('_partials.newest-article')
        </div>
    </div>
</section>
@endsection
