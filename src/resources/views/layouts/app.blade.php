<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @stack('meta')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Teacher Wellbeing</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @stack('styles')
</head>
<body>
    <div id="app">
        @include('_partials.navbar')
        @yield('content')
        @include('_partials.footer')

        <!-- Modal -->
        <div class="modal fade" id="selectImage" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-block mb-5">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="container-fluid mb-4">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h4 class="font-weight-bold mb-5" v-if="!isUploadingImage">Search By Image</h4>
                                    <h4 class="font-weight-bold mb-5" v-else>Processing Image</h4>
                                </div>
                                <div class="col-lg-12">
                                    <form action="">
                                        <div class="form-group" v-if="!isUploadingImage">
                                            <input type="file" name="image" class="d-none" id="localimage" @change="uploadImage">
                                            <label class="btn btn-outline-primary btn-block" for="localimage"><i class="fas fa-image mr-2"></i> Upload Photo</label>
                                        </div>
                                        <div class="d-flex justify-content-center" v-else>
                                            <div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <input type="text" name="" class="form-control rounded-pill p-4" id="urlImage" v-if="showUrlImage" placeholder="Paste Image URL">
                                            <label class="btn btn-outline-primary btn-block" for="urlImage" @click="showUrlImage = true" v-else><i class="fas fa-link mr-2"></i> Paste Image URL</label>
                                        </div> --}}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
