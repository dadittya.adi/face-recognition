@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Ketentuan Pengguna',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        @include('_partials.dashboard-sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-green font-weight-bold d-flex align-items-center">Kebijakan Privasi</h5>
                    <hr>
                    <h5 class="text-green font-weight-bold">Lorem Ipsum dolor sio amet</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elementum augue quis ex tincidunt commodo. Aenean aliquam eleifend orci, eu commodo augue ornare at. Morbi ultrices lobortis diam in faucibus. Donec nec rhoncus urna. Nunc nulla magna, suscipit sagittis ante congue, porttitor rhoncus risus. Donec dictum aliquam ultricies. Vestibulum est augue, pellentesque ac purus laoreet, pharetra bibendum magna. Donec bibendum aliquet ipsum, ac lacinia sapien congue sit amet. Nunc eget purus vel felis volutpat convallis in non turpis. Nulla accumsan leo ut augue mattis, vitae hendrerit purus laoreet. Vestibulum non diam in risus mollis vulputate. Integer rhoncus vulputate nisl, at tristique orci dignissim eu. Nam et neque turpis. Curabitur ac libero quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus, odio a mattis condimentum, lectus odio laoreet lacus, nec suscipit sem velit ac nibh. Nunc mi tortor, ullamcorper sit amet ante ut, tempor porta libero. Praesent pharetra placerat sapien, non condimentum augue pretium quis. Cras et dolor non ex auctor venenatis.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
