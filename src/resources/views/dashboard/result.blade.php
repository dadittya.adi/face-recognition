@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Hasil Tes',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-body p-5 rounded-lg">
                    <div class="text-center mb-5">
                        <p class="font-weight-bold text-md">Terima kasih, anda telah mengisi semua kuesioner</p>
                        <h3 class="font-weight-bold text-green mb-0">Profil Kompetensi Anda</h3>
                    </div>
                    <div class="mb-5">
                        <canvas id="big-chart-area" class="chartjs-render-monitor" style="width:800px;height:800px" width="800" height="800"></canvas>
                    </div>

                    <div class="mb-5">
                        <h5 class="font-weight-bold text-green text-lg text-center mb-5">Keterangan Elemen Kompetensi</h5>
                        <div class="row">
                            @foreach($dimensions as $key => $dimension)
                                <div class="col-lg-6">
                                    <h6 class="font-weight-bold">{{ $dimension->name }}</h6>
                                    <p>
                                        @foreach ($dimension->indicator()
                                        ->where('is_active',true)
                                        ->orderby('id','asc')
                                        ->get() as $key_2 => $indicator)
                                        Indikator {{ ($key_2+1) }}: {{ $indicator->name }} <br>
                                        @endforeach
                                    </p>
                                </div>
                            @endforeach

                            <!--<div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi IT</h6>
                                <p>
                                    Indikator 1: Mastery of Technology Access <br>
                                    Indikator 2: Mastery of Technology Media Skills
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Kepribadian</h6>
                                <p>
                                    Indikator 1: Value <br>
                                    Indikator 2: Work Ethic
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Impact and Influence</h6>
                                <p>
                                    Indikator 1: Sustainable Mindset <br>
                                    Indikator 2: Personalized Power
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Sosial</h6>
                                <p>
                                    Indikator 1: Customer Service Orientation <br>
                                    Indikator 2: Relationship Building
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Achievement Orientation</h6>
                                <p>
                                    Indikator 1: Ability to Work Under Pressure <br>
                                    Indikator 2: Focus on Improvement
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Profesional</h6>
                                <p>
                                    Indikator 1: Task Performance <br>
                                    Indikator 2: Reflective Action
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Nasionalisme</h6>
                                <p>
                                    Indikator 1: Prioritizing the Nation <br>
                                    Indikator 2: Accepting Pluralism
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Metodologi</h6>
                                <p>
                                    Indikator 1: Problem Solving <br>
                                    Indikator 2: Research Skill
                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h6 class="font-weight-bold">Kompetensi Developing Others</h6>
                                <p>
                                    Indikator 1: Leadership Skills <br>
                                    Indikator 2: Ability to Transfer Knowledge
                                </p>
                            </div-->
                        </div>
                    </div>

                    <div class="mb-5">
                        <h5 class="font-weight-bold text-green text-lg text-center mb-4">Rekomendasi Video Pembelanjaran</h5>
                        <p>Berikut ini adalah rekomendasi pembelanjan untuk meningkatkan Kompetensi anda :</p>
                        <iframe width="100%" height="350" src="https://www.youtube.com/embed/o9PEzWFLwBc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    <div class="mb-5 d-flex align-items-center">
                        <a href="{{ route('home') }}">Kembali Ke Beranda</a>
                        <div class="ml-auto">
                            <a href="{{ route('history') }}" class="btn btn-outline-primary">Lihat Histori Tes</a>
                            <a href="{{ route('quiz') }}" class="btn btn-primary">Ulangi Tes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" defer></script>
    <script src="{{ asset('js/chartjs-plugin-labels.js') }}" defer></script>
    <script>

        var dimension_value = [];
        var indicator_value = [];

        chart();

        async function getData() {
            axios.get('total-count-dimension')
            .then(response => {
                response.data.forEach(item => {
                    dimension_value.push(parseFloat(item));
                });
            });

            axios.get('total-count-indicator')
            .then(response => {
                response.data.forEach(item => {
                    indicator_value.push(parseFloat(item));
                });
            });
        }

        async function chart() {
            await getData();

            var config = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: dimension_value,
                        backgroundColor: [
                            '#C21400',
                            '#FDC126',
                            '#24AFF2',
                            '#52832F',
                            '#712AA2',
                            '#18B6C2',
                            '#EF7D23',
                            '#90D346',
                            '#FE1E01',
                            '#FDFE35',
                        ],
                        labels: [
                            'Kompetensi \nPedagogic',
                            'Kompetensi \nKepribadian',
                            'Kompetensi \nSosial',
                            'Kompetensi \nProfesional',
                            'Kompetensi \nMethodology',
                            'Kompetensi \nIT',
                            'Kompetensi \nImpact & Influence',
                            'Kompetensi \nAchievement Orientation',
                            'Kompetensi \nNasionalism',
                            'Kompetensi \nDeveloping Others',
                        ]
                    },{
                        data: indicator_value,
                        backgroundColor: [
                            '#EDB2B2',
                            '#DA6666',
                            '#FEECB2',
                            '#FDDA66',
                            '#B2E7FB',
                            '#66CFF7',
                            '#CBDAC0',
                            '#97B582',
                            '#D4BFE3',
                            '#AA7FC7',

                            '#B2E9ED',
                            '#66D3DA',
                            '#FAD8BC',
                            '#F5B17B',
                            '#DDF2C7',
                            '#BCE58F',
                            '#FFB2B2',
                            '#FE6666',
                            '#FEFFB2',
                            '#FDFF66',
                        ],
                        labels: [
                            'MLT',
                            'MLM',
                            'VAL',
                            'WET',
                            'CSO',
                            'RBD',
                            'TPF',
                            'RFA',
                            'PSO',
                            'RSK',
                            'MTA',
                            'MTM',
                            'SMS',
                            'PPO',
                            'AWU',
                            'FOI',
                            'PTN',
                            'APL',
                            'LDS',
                            'ATK',
                        ]
                    }],
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        usePointStyle: true,
                    },
                    title: {
                        display: false,
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var index = tooltipItem.index;
                                return dataset.labels[index] + ': ' + dataset.data[index];
                            }
                        }
                    },
                    plugins: {
                        labels: {
                            render: 'label',
                            fontColor: '#000000',
                            arc: true
                        }
                    },
                }
            };

            window.onload = function() {
                var ctx = document.getElementById('big-chart-area').getContext('2d');
                window.myBigDoughnut = new Chart(ctx, config);
            };
        }
    </script>
@endpush
