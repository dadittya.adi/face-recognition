@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Ubah Kata Sandi',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        @include('_partials.dashboard-sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-green font-weight-bold d-flex align-items-center">Ubah Kata Sandi</h5>
                    <hr>
                    <form action="{{ route('password.update') }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group form-row">
                            <div class="col-lg-6">
                                <label for="oldpassword" class="font-weight-bold">Kata Sandi Lama</label>
                                <input type="password" class="form-control" id="oldpassword" name="oldpassword">
                                @if( $errors->has('oldpassword') )
                                    <small class="text-danger">{{ $errors->first('oldpassword') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col-lg-6">
                                <label for="password" class="font-weight-bold">Kata Sandi Baru</label>
                                <input type="password" class="form-control" id="password" name="password">
                                @if( $errors->has('password') )
                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                @else
                                    <small class="form-text text-muted">Gunakan minimal 8 karakter, dan setidaknya satu huruf dan satu angka</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col-lg-6">
                                <label for="password_confirmation" class="font-weight-bold">Ulangi Kata Sandi Baru</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                @if( $errors->has('password_confirmation') )
                                    <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
