@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Histori Tes',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        @include('_partials.dashboard-sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-green font-weight-bold d-flex align-items-center">Riwayat Hasil Tes</h5>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <p class="mb-0">Untuk mengetahui peningkatan Kompetensi Anda, <br>Yuk ulangi Tes Teacher Wellbeing!</p>
                        <a href="{{ route('quiz') }}" class="btn btn-primary">Tes Ulang</a>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Tes</th>
                                <th>Status Pembayaran</th>
                                <th>Status Tes</th>
                                <th>Lihat Hasil Tes</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $test)
                            <tr>
                                <td>{{ $test->test_number }}</td>
                                <td>{{ date('d M Y', strtotime($test->test_date)) }}</td>
                                <td class="text-green">{{ $test->type }}</td>
                                <td>{{ $test->test_state }}</td>
                                <td><a href="{{ route('result') }}" target="_blank"><i class="fas fa-file-alt"></i> Lihat Hasil</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
