<?php

return [
    'gallery'       => storage_path() . '/app/gallery',
    'researcher'    => storage_path() . '/app/researcher',
    'article'       => storage_path() . '/app/article',
    'publication'   => storage_path() . '/app/publication',
    'upload'        => storage_path() . '/app/upload'
];
