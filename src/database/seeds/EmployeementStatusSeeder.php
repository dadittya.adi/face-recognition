<?php

use Illuminate\Database\Seeder;

use App\Models\EmploymentStatus;

class EmployeementStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmploymentStatus::create([
            'name'      => 'Guru Tetap PNS',
            'is_active' => true
        ]);

        EmploymentStatus::create([
            'name'      => 'Guru Tetap Yayasan',
            'is_active' => true
        ]);

        EmploymentStatus::create([
            'name'      => 'Honorer',
            'is_active' => true
        ]);
    }
}
