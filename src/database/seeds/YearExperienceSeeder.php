<?php

use Illuminate\Database\Seeder;

use App\Models\YearsExperience;

class YearExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        YearsExperience::create([
            'name' => '<5 tahun',
            'is_active' => true,
        ]);
        YearsExperience::create([
            'name' => '5 - 10 tahun',
            'is_active' => true,
        ]);
        YearsExperience::create([
            'name' => '11 -  20 tahun',
            'is_active' => true,
        ]);
        YearsExperience::create([
            'name' => '>20 tahun',
            'is_active' => true,
        ]);
    }
}
