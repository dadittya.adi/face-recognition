<?php use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Models\Admin;
use App\Models\User;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin                       = new Admin();
        $admin->name                 = 'admin';
        $admin->email                = 'admin@kompetensi-guru.com';
        $admin->email_verified_at    = carbon::now();
        $admin->password             = bcrypt('password1');
        $admin->save();

        $user                       = new User();
        $user->name                 = 'robertde djumadilakir';
        $user->email                = 'robertdedjumadilakir@gmail.com';
        $user->email_verified_at    = carbon::now();
        $user->password             = bcrypt('password1');
        $user->save();
    }
}
