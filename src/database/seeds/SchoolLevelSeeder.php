<?php

use Illuminate\Database\Seeder;


use App\Models\SchoolLevel;

class SchoolLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolLevel::create([
            'name' =>'TK',
            'is_active' => true,
        ]);

        SchoolLevel::create([
            'name' =>'SD',
            'is_active' => true,
        ]);
        SchoolLevel::create([
            'name' =>'SMP',
            'is_active' => true,
        ]);
        SchoolLevel::create([
            'name' =>'SMA',
            'is_active' => true,
        ]);
        SchoolLevel::create([
            'name' =>'SMK',
            'is_active' => true,
        ]);
    }
}
