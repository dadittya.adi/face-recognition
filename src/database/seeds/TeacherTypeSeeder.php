<?php

use Illuminate\Database\Seeder;

use App\Models\TeacherType;

class TeacherTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeacherType::create([
            'name' => 'Guru Kelas',
            'is_active' => true,
        ]);
        TeacherType::create([
            'name' => 'Guru Mapel',
            'is_active' => true,
        ]);
        TeacherType::create([
            'name' => 'Guru BK',
            'is_active' => true,
        ]);
        TeacherType::create([
            'name' => 'Lainnya',
            'is_active' => true,
        ]);
    }
}
