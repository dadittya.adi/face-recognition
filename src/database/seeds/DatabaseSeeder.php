<?php use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        //DB::table('admins')->truncate();
        $this->call(AdminTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(AgeCategorySeeder::class);
        $this->call(EmployeementStatusSeeder::class);
        $this->call(SchoolLevelSeeder::class);
        $this->call(EducationSeeder::class);
        $this->call(YearExperienceSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(TeacherTypeSeeder::class);
        $this->call(AdditionalTaskSeeder::class);

    }
}
