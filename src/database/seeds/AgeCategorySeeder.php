<?php use Illuminate\Database\Seeder;

use App\Models\AgeCategory;

class AgeCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AgeCategory::create([
            'name'      => '<30 tahun',
            'is_active' => true,
        ]);

        AgeCategory::create([
            'name'      => '30 - 40 tahun',
            'is_active' => true,
        ]);

        AgeCategory::create([
            'name'      => '41 - 50 tahun',
            'is_active' => true,
        ]);

        AgeCategory::create([
            'name'      => '>50 tahun',
            'is_active' => true,
        ]);
    }
}
