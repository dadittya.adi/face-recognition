<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('gender')->nullable();
            $table->unsignedInteger('age_category_id')->nullable();
            $table->unsignedInteger('education_id')->nullable();
            $table->char('nuptk', 16)->nullable();
            $table->char('nik', 18)->nullable();
            $table->unsignedInteger('employement_status_id')->nullable();
            $table->unsignedInteger('school_level_id')->nullable();
            $table->string('school_name')->nullable();
            $table->enum('school_status', ['Negeri', 'Swasta'])->nullable();
            $table->unsignedInteger('teacher_type_id')->nullable();
            $table->unsignedInteger('subject_id')->nullable();
            $table->unsignedInteger('years_experience_id')->nullable();
            $table->enum('certification', ['Telah Sertifikasi', 'Belum Sertifikasi'])->nullable();
            $table->unsignedInteger('additional_task_id')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->timestamps();

            $table->foreign('age_category_id')->references('id')->on('age_categories');
            $table->foreign('education_id')->references('id')->on('educations');
            $table->foreign('employement_status_id')->references('id')->on('employement_status');
            $table->foreign('school_level_id')->references('id')->on('school_levels');
            $table->foreign('teacher_type_id')->references('id')->on('teacher_types');
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->foreign('years_experience_id')->references('id')->on('years_experience');
            $table->foreign('additional_task_id')->references('id')->on('additional_tasks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
