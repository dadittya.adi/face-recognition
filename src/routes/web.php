<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/fetch-questionnaire', 'HomeController@fetchQuestionnaire');
Route::get('/total-count-dimension', 'HomeController@totalCountDimension');
Route::get('/total-count-indicator', 'HomeController@totalCountIndicator');
Route::post('/detect-emotion', 'LandingController@detectEmotion')->name('detect.emotion');
Route::get('/detect-emotion', 'LandingController@detectEmotionPage')->name('detect.emotion.page');
Route::get('/upload/image/{slug}', 'LandingController@showUploadedImage')->name('show.image');

Route::group(['as' => 'landing.'], function() {

    // Homepage
    Route::get('/', 'LandingController@index')->name('home');

    // Artikel
    Route::get('/artikel', 'LandingController@articles')->name('artikel');
    Route::get('/artikel/{slug}', 'LandingController@showArticle')->name('artikel.single');

    // Publikasi
    Route::get('/publikasi', 'LandingController@publications')->name('publikasi');
    Route::get('/publikasi/{slug}', 'LandingController@showPublication')->name('publikasi.single');

    // Informasi
    Route::get('/informasi/hki', 'LandingController@hki')->name('hki');
    Route::get('/informasi/profile-peneliti', 'LandingController@researchist')->name('profile');
    Route::get('/informasi/profile-peneliti/{slug}', 'LandingController@showProfile')->name('profile.show');
    Route::get('/informasi/faq', 'LandingController@faq')->name('faq');

    // Galeri
    Route::get('/gallery', 'LandingController@gallery')->name('gallery');

    // About
    Route::get('/about', 'LandingController@about')->name('about');

    // Image URL
    Route::get('/image/{page}/{image}', 'LandingController@showImage')->name('image.show');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/quiz', 'HomeController@quiz')->name('quiz');
Route::get('/result', 'HomeController@result')->name('result');
Route::get('/history', 'HomeController@history')->name('history');
Route::get('/schools', 'HomeController@schools')->name('schools');
Route::get('/change-password', 'HomeController@changePassword')->name('changePassword');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/policy', 'HomeController@policy')->name('policy');
Route::post('/send-answer', 'HomeController@storeAnswer')->name('answer.store');
Route::put('/update/account', 'HomeController@updateAccount')->name('account.update');
Route::put('/update/school', 'HomeController@updateSchool')->name('school.update');
Route::put('/update/password', 'HomeController@updatePassword')->name('password.update');



Route::group(['prefix' => 'backend', 'namespace' => 'Backend'], function ()
{
    Route::get('/', function ()
    {
        return redirect('/backend/dashboard');
    });

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('backend.login');
    /**/
    Route::get('/dashboard', 'HomeController@index')->name('backend.home');
    Route::get('/account-setting', 'HomeController@accountSetting')->name('backend.accountSetting');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('backend.forgot_password');
    Route::get('/password/reset/{id}/{date}/{token}', 'Auth\ResetPasswordController@showResetForm')->name('backend.resetpassword');

    Route::post('/login', 'Auth\LoginController@doLogin')->name('backend.doLogin');
    Route::post('/logout', 'Auth\LoginController@doLogout')->name('backend.doLogout');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('backend.sendlink');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('backend.reset');

    Route::prefix('admin')->group(function()
    {
        Route::get('', 'AdminController@index')->name('backend.admin.index');
        Route::get('data', 'AdminController@data')->name('backend.admin.data');
        Route::get('edit/{id}', 'AdminController@edit')->name('backend.admin.edit');
        Route::post('store', 'AdminController@store')->name('backend.admin.store');
        Route::post('update/{id}', 'AdminController@update')->name('backend.admin.update');
        Route::delete('delete/{id}', 'AdminController@delete')->name('backend.admin.delete');
    });

    Route::prefix('age-category')->group(function()
    {
        Route::get('', 'AgeCategoryController@index')->name('backend.ageCategory.index');
        Route::get('data', 'AgeCategoryController@data')->name('backend.ageCategory.data');
        Route::get('edit/{id}', 'AgeCategoryController@edit')->name('backend.ageCategory.edit');
        Route::post('store', 'AgeCategoryController@store')->name('backend.ageCategory.store');
        Route::post('update/{id}', 'AgeCategoryController@update')->name('backend.ageCategory.update');
        Route::delete('delete/{id}', 'AgeCategoryController@delete')->name('backend.ageCategory.delete');
    });

    Route::prefix('education')->group(function()
    {
        Route::get('', 'EducationController@index')->name('backend.education.index');
        Route::get('data', 'EducationController@data')->name('backend.education.data');
        Route::get('edit/{id}', 'EducationController@edit')->name('backend.education.edit');
        Route::post('store', 'EducationController@store')->name('backend.education.store');
        Route::post('update/{id}', 'EducationController@update')->name('backend.education.update');
        Route::delete('delete/{id}', 'EducationController@delete')->name('backend.education.delete');
    });

    Route::prefix('employement-status')->group(function()
    {
        Route::get('', 'EmploymentStatusController@index')->name('backend.employementStatus.index');
        Route::get('data', 'EmploymentStatusController@data')->name('backend.employementStatus.data');
        Route::get('edit/{id}', 'EmploymentStatusController@edit')->name('backend.employementStatus.edit');
        Route::post('store', 'EmploymentStatusController@store')->name('backend.employementStatus.store');
        Route::post('update/{id}', 'EmploymentStatusController@update')->name('backend.employementStatus.update');
        Route::delete('delete/{id}', 'EmploymentStatusController@delete')->name('backend.employementStatus.delete');
    });

    Route::prefix('school-level')->group(function()
    {
        Route::get('', 'SchoolLevelController@index')->name('backend.schoolLevel.index');
        Route::get('data', 'SchoolLevelController@data')->name('backend.schoolLevel.data');
        Route::get('edit/{id}', 'SchoolLevelController@edit')->name('backend.schoolLevel.edit');
        Route::post('store', 'SchoolLevelController@store')->name('backend.schoolLevel.store');
        Route::post('update/{id}', 'SchoolLevelController@update')->name('backend.schoolLevel.update');
        Route::delete('delete/{id}', 'SchoolLevelController@delete')->name('backend.schoolLevel.delete');
    });

    Route::prefix('teacher-type')->group(function()
    {
        Route::get('', 'TeacherTypeController@index')->name('backend.teacherType.index');
        Route::get('data', 'TeacherTypeController@data')->name('backend.teacherType.data');
        Route::get('edit/{id}', 'TeacherTypeController@edit')->name('backend.teacherType.edit');
        Route::post('store', 'TeacherTypeController@store')->name('backend.teacherType.store');
        Route::post('update/{id}', 'TeacherTypeController@update')->name('backend.teacherType.update');
        Route::delete('delete/{id}', 'TeacherTypeController@delete')->name('backend.teacherType.delete');
    });

    Route::prefix('subject')->group(function()
    {
        Route::get('', 'subjectController@index')->name('backend.subject.index');
        Route::get('data', 'subjectController@data')->name('backend.subject.data');
        Route::get('data-shool-level', 'subjectController@dataSchoolLevel')->name('backend.subject.dataSchoolLevel');
        Route::get('edit/{id}', 'subjectController@edit')->name('backend.subject.edit');
        Route::post('store', 'subjectController@store')->name('backend.subject.store');
        Route::post('update/{id}', 'subjectController@update')->name('backend.subject.update');
        Route::delete('delete/{id}', 'subjectController@delete')->name('backend.subject.delete');
    });

    Route::prefix('year-experience')->group(function()
    {
        Route::get('', 'YearExperienceController@index')->name('backend.yearExperience.index');
        Route::get('data', 'YearExperienceController@data')->name('backend.yearExperience.data');
        Route::get('edit/{id}', 'YearExperienceController@edit')->name('backend.yearExperience.edit');
        Route::post('store', 'YearExperienceController@store')->name('backend.yearExperience.store');
        Route::post('update/{id}', 'YearExperienceController@update')->name('backend.yearExperience.update');
        Route::delete('delete/{id}', 'YearExperienceController@delete')->name('backend.yearExperience.delete');
    });

    Route::prefix('additional-task')->group(function()
    {
        Route::get('', 'AdditionalTaskController@index')->name('backend.additionalTask.index');
        Route::get('data', 'AdditionalTaskController@data')->name('backend.additionalTask.data');
        Route::get('edit/{id}', 'AdditionalTaskController@edit')->name('backend.additionalTask.edit');
        Route::post('store', 'AdditionalTaskController@store')->name('backend.additionalTask.store');
        Route::post('update/{id}', 'AdditionalTaskController@update')->name('backend.additionalTask.update');
        Route::delete('delete/{id}', 'AdditionalTaskController@delete')->name('backend.additionalTask.delete');
    });

    Route::prefix('dimension')->group(function()
    {
        Route::get('', 'DimensionController@index')->name('backend.dimension.index');
        Route::get('data', 'DimensionController@data')->name('backend.dimension.data');
        Route::get('edit/{id}', 'DimensionController@edit')->name('backend.dimension.edit');
        Route::post('store', 'DimensionController@store')->name('backend.dimension.store');
        Route::post('update/{id}', 'DimensionController@update')->name('backend.dimension.update');
        Route::delete('delete/{id}', 'DimensionController@delete')->name('backend.dimension.delete');
    });

    Route::prefix('album')->group(function()
    {
        Route::get('', 'AlbumController@index')->name('backend.album.index');
        Route::get('data', 'AlbumController@data')->name('backend.album.data');
        Route::get('edit/{id}', 'AlbumController@edit')->name('backend.album.edit');
        Route::post('store', 'AlbumController@store')->name('backend.album.store');
        Route::post('update/{id}', 'AlbumController@update')->name('backend.album.update');
        Route::delete('delete/{id}', 'AlbumController@delete')->name('backend.album.delete');
    });

    Route::prefix('gallery')->group(function()
    {
        Route::get('', 'GalleryController@index')->name('backend.gallery.index');
        Route::get('data', 'GalleryController@data')->name('backend.gallery.data');
        Route::get('create', 'GalleryController@create')->name('backend.gallery.create');
        Route::get('data-album', 'GalleryController@dataAlbum')->name('backend.gallery.dataAlbum');
        Route::get('show-image/{id}', 'GalleryController@showImage')->name('backend.gallery.showImage');
        Route::get('edit/{id}', 'GalleryController@edit')->name('backend.gallery.edit');
        Route::post('store', 'GalleryController@store')->name('backend.gallery.store');
        Route::post('update/{id}', 'GalleryController@update')->name('backend.gallery.update');
        Route::delete('delete/{id}', 'GalleryController@delete')->name('backend.gallery.delete');
    });

    Route::prefix('researcher')->group(function()
    {
        Route::get('', 'ResearcherController@index')->name('backend.researcher.index');
        Route::get('data', 'ResearcherController@data')->name('backend.researcher.data');
        Route::get('create', 'ResearcherController@create')->name('backend.researcher.create');
        Route::get('edit/{id}', 'ResearcherController@edit')->name('backend.researcher.edit');
        Route::get('show-image/{image}', 'ResearcherController@showImage')->name('backend.researcher.showImage');
        Route::post('store', 'ResearcherController@store')->name('backend.researcher.store');
        Route::post('update/{id}', 'ResearcherController@update')->name('backend.researcher.update');
        Route::delete('delete/{id}', 'ResearcherController@delete')->name('backend.researcher.delete');
    });

    Route::prefix('article')->group(function()
    {
        Route::get('', 'ArticleController@index')->name('backend.article.index');
        Route::get('data', 'ArticleController@data')->name('backend.article.data');
        Route::get('create', 'ArticleController@create')->name('backend.article.create');
        Route::get('edit/{id}', 'ArticleController@edit')->name('backend.article.edit');
        Route::get('show-image/{image}', 'ArticleController@showImage')->name('backend.article.showImage');
        Route::post('store', 'ArticleController@store')->name('backend.article.store');
        Route::post('update/{id}', 'ArticleController@update')->name('backend.article.update');
        Route::delete('delete/{id}', 'ArticleController@delete')->name('backend.article.delete');
    });

    Route::prefix('publication')->group(function()
    {
        Route::get('', 'PublicationController@index')->name('backend.publication.index');
        Route::get('data', 'PublicationController@data')->name('backend.publication.data');
        Route::get('create', 'PublicationController@create')->name('backend.publication.create');
        Route::get('edit/{id}', 'PublicationController@edit')->name('backend.publication.edit');
        Route::get('show-file/{file}', 'PublicationController@showFile')->name('backend.publication.showFile');
        Route::post('store', 'PublicationController@store')->name('backend.publication.store');
        Route::post('update/{id}', 'PublicationController@update')->name('backend.publication.update');
        Route::delete('delete/{id}', 'PublicationController@delete')->name('backend.publication.delete');
    });

    Route::prefix('questionnaire')->group(function()
    {
        Route::get('', 'QuestionnaireController@index')->name('backend.questionnaire.index');
        Route::get('data', 'QuestionnaireController@data')->name('backend.questionnaire.data');
        Route::get('data-dimension', 'QuestionnaireController@dataDimension')->name('backend.gallery.dataDimension');
        Route::get('data-indicator', 'QuestionnaireController@dataIndicator')->name('backend.gallery.dataIndicator');
        Route::get('import', 'QuestionnaireController@import')->name('backend.questionnaire.import');
        Route::get('download-form-import', 'QuestionnaireController@downloadFormImport')->name('backend.questionnaire.downloadFormImport');
        Route::get('edit/{id}', 'QuestionnaireController@edit')->name('backend.questionnaire.edit');
        Route::post('upload-form-import', 'QuestionnaireController@uploadFormImport')->name('backend.questionnaire.uploadFormImport');
        Route::post('update/{id}', 'QuestionnaireController@update')->name('backend.questionnaire.update');
        Route::delete('delete/{id}', 'QuestionnaireController@delete')->name('backend.questionnaire.delete');
    });

    Route::prefix('test-result')->group(function()
    {
        Route::get('', 'TestResultController@index')->name('backend.testResult.index');
        Route::get('download-form-import', 'TestResultController@downloadReport')->name('backend.testResult.downloadReport');
    });
});
