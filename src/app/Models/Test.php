<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table        = 'tests';
    protected $fillable     = ['user_id'
        ,'test_number'
        ,'test_date'
        ,'type'
        ,'test_state'
        ,'status'
        ,'price'
        ,'paid_at'
    ];

    static function generateTestNumber()
    {
        $try        = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized test number");

            $test_number = strtoupper(static::cleanFilename(str_random(7)));
            
            $try -= 1;
        } while (static::isTestNumberExist($test_number));

        return $test_number;
    }

    static function isTestNumberExist($test_number)
    {
        return Test::where('test_number',$test_number)->exists();
    }

    static function cleanFilename($value)
    {
        $pattern        = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement    = '-';
        $value          = preg_replace($pattern, $replacement, $value);

        return $value;
    }
}
