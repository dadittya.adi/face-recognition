<?php namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $table        = 'publications';
    protected $dates        = ['published_date'];
    protected $fillable     = ['title'
        ,'published_date'
        ,'slug'
        ,'abstract'
        ,'file'
        ,'is_active'
    ];

    static function getFileFullPath($image)
    {
        return Config::get('storage.publication') . '/' . e($image);
    }

    static function generateFileName($image)
    {
        $ret        = [];
        $path       = Config::get('storage.researcher');
        $extension  = $image->getClientOriginalExtension();
        $try        = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
                
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value)
    {
        $pattern        = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement    = '-';
        $value          = preg_replace($pattern, $replacement, $value);

        return $value;
    }
}
