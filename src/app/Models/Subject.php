<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table        = 'subjects';
    protected $fillable     = ['name'
        ,'school_level_id'
        ,'is_active'
    ];

    public function schoolLevel()
    {
        return $this->belongsTo('App\Models\SchoolLevel','school_level_id');
    }
}
