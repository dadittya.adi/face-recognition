<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolLevel extends Model
{
    protected $table        = 'school_levels';
    protected $fillable     = ['name'
        ,'is_active'
    ];

    public function subject()
    {
        return $this->hasMany('App\Models\Subject');
    }
}
