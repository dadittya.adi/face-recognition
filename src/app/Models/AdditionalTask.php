<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalTask extends Model
{
    protected $table        = 'additional_tasks';
    protected $fillable     = ['name'
        ,'is_active'
    ];
}
