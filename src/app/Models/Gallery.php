<?php namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table        = 'galleries';
    protected $fillable     = ['album_id'
        ,'description'
        ,'image'
    ];

    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    static function getImageFullPath($image)
    {
        return Config::get('storage.gallery') . '/' . e($image);
    }

    static function showImage($filename)
    {
        $path = Config::get('storage.gallery');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    static function generateFileName($image)
    {
        $ret        = [];
        $path       = Config::get('storage.gallery');
        $extension  = $image->getClientOriginalExtension();
        $try        = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;

            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value)
    {
        $pattern        = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement    = '-';
        $value          = preg_replace($pattern, $replacement, $value);

        return $value;
    }
}
