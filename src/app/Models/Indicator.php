<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $table        = 'indicators';
    protected $fillable     = ['dimension_id'
        ,'name'
        ,'is_active'
    ];

    public function getNameAttribute($value)
	{
	    return ucwords($value);
    }

    public function dimension()
    {
        return $this->belongsTo('App\Models\Dimension','dimension_id');
    }

    public function questionnaire()
    {
        return $this->hasMany('App\Models\Questionnaire');
    }

    static function getTotal($id,$user_id)
    {
        return DB::select(db::raw("
            select indicators.id,
            COALESCE(sum(dtl.total),'0') as total from indicators
            left join (
                select questionnaires.indicator_id,sum(COALESCE(dtl.total,'0'))as total
                From questionnaires
                left join (
                        select questionnaire_id,
                        count(0) as total
                        from test_questionnaire
                        where answer = 't'
                        and exists(
                                select 1 
                                from tests
                                where tests.id = test_questionnaire.test_id
                                and user_id = $user_id
                        )
                        GROUP BY questionnaire_id
                ) dtl on dtl.questionnaire_id = questionnaires.id
                where questionnaires.is_active = 'y'
                GROUP BY questionnaires.indicator_id
            ) dtl on dtl.indicator_id = indicators.id
            where indicators.id = $id
            and indicators.is_active = 'y'
            GROUP BY indicators.id
            order by dimension_id asc
        "));
    }
}
