<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestQuestionnaire extends Model
{
    protected $table        = 'test_questionnaire';
    protected $fillable     = ['questionnaire_id'
        ,'test_id'
        ,'answer'
    ];

    public function test()
    {
        return $this->belongsTo('App\Models\Test','test_id');
    }

    public function questionnaire()
    {
        return $this->belongsTo('App\Models\Questionnaire','questionnaire_id');
    }
}
