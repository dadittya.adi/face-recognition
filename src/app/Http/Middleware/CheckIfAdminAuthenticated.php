<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = Auth::guard('admins');
        
        if (!$auth->check()) 
        {
            return redirect('/backend/login');
        }

        
        if (Auth::guard('admins')->user()->email_verified_at == null) 
        {
            Auth::guard('admins')->user()->logout();
            
            return redirect()->action('Backend\Auth\LoginController@showLoginForm')->with('msg', 'This Account not active yet');
        }

        return $next($request);
    }
}
