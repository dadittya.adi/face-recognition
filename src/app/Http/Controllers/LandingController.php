<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Album;
use App\Models\Articles;
use App\Models\Publication;
use App\Models\Researchist;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Excep;

class LandingController extends Controller
{

    public $api_url = '';

    public function __construct()
    {
        $this->api_url = Config::get('app.api_url');
    }

    public function index()
    {
        $articles = Articles::where('is_active',true)
                            ->orderBy('id','desc')
                            ->paginate(3);

        return view('landing.home', compact('articles'));
    }

    public function articles()
    {
        $articles = Articles::where('is_active',true)
                            ->orderBy('id','desc')
                            ->paginate(6);

        return view('landing.article', compact('articles'));
    }

    public function showArticle($slug)
    {
        $article = Articles::where('slug', $slug)->first();

        return view('landing.single-article', compact('article'));
    }

    public function publications()
    {
        $publications = Publication::orderby('updated_at','desc')
                                    ->where('is_active',true)
                                    ->paginate(10);

        return view('landing.publikasi', compact('publications'));
    }

    public function showPublication($slug)
    {
        $publication = Publication::where('slug',$slug)->first();

        return view('landing.single-publikasi', compact('publication'));
    }

    public function hki()
    {
        return view('landing.hki');
    }

    public function researchist()
    {
        $researchists = Researchist::paginate(3);
        return view('landing.researchist', compact('researchists'));
    }

    public function faq()
    {
        return view('landing.faq');
    }

    public function gallery()
    {
        $galleries = Album::paginate(9);
        return view('landing.gallery', compact('galleries'));
    }

    public function about()
    {
        return view('landing.about');
    }

    public function showProfile()
    {
        return view('landing.about');
    }

    public function showImage($page, $filename)
    {
        /**
         * ========================================
         * Storage Name
         * ========================================
         * - gallery
         * - researcher
         * - article
         * - publication
         */

        $path = Config::get('storage.'.$page);
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        return $resp;
    }

    public function showUploadedImage($filename)
    {
        $path = Config::get('storage.upload');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        return $resp;
    }

    public function detectEmotionPage()
    {
        return view('landing.result_test');
    }


    public function detectEmotion(Request $request)
    {
        $storage    = Config::get('storage.upload');
        $image      = $request->file('image');

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        if($image)
        {
            $random         = static::generateFileName($image);
            //$_random        = $random[1];
        }

        $image->move($storage, $random[1]);

        $path = $storage . '/' . $random[1];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        $headers = array('Content-Type: multipart/form-data');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);

        if($path <> ''){
            $post['image'] = new \CURLFile(realpath($path));
        }
	    else {
            $post['image'] = '';
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $emotionResponse = curl_exec($ch);
        curl_close ($ch);

        $emotionResponse = json_decode($emotionResponse);
        $emotionResponse->images = (object) [route('show.image', $random[1])];
        $emotionResponse = json_encode($emotionResponse);

        return response($emotionResponse);
    }
    static function generateFileName($image)
    {
        $ret        = [];
        $path       = Config::get('storage.upload');
        $extension  = $image->getClientOriginalExtension();
        $try        = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;

            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value)
    {
        $pattern        = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement    = '-';
        $value          = preg_replace($pattern, $replacement, $value);

        return $value;
    }
}
