<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;

class MapsController extends Controller
{
    public function list()
    {
        $provinces = \Indonesia::allProvinces();
        $data = User::get()->groupBy('province');

        $json = [];

        foreach( $provinces as $key => $province ) {

            $path_number = $key >= 9 ? ($key + 1) : "0".($key + 1);

            $amount = isset($data[$province->id]) ? $data[$province->id]->count() : 0;

            // Schools
            if  ( $amount > 0 ) {
                $schools = $data[$province->id]->groupBy('school_name')->count();
            } else {
                $schools = 0;
            }

            $arr = [
                "path" => "path".$path_number,
                "provinsi" => $province->name,
                "guru" => $amount,
                "sekolah" => $schools,
                "yayasan" => $schools
            ];

            $json[$key] = $arr;

        }

        return response()->json($json, 200);
    }
}
