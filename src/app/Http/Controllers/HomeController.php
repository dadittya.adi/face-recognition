<?php namespace App\Http\Controllers;

use Auth;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;

use App\Models\Test;
use App\Models\Dimension;
use App\Models\Indicator;
use App\Models\Questionnaire;
use App\Models\TestQuestionnaire;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['fetchQuestionnaire','totalCountDimension','totalCountIndicator']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard.home');
    }

    public function fetchQuestionnaire(Request $request)
    {
        $data = Questionnaire::where('is_active','t')
        ->orderby('order','asc')
        ->get();

        $attrs = [];
        foreach ($data as $key => $value)
        {
            $obj = new stdClass();
            $obj->id = $value->id;
            $obj->name = $value->name;
            $attrs[$value->order][] = $obj;
        }

        $paginate = 10;
        $page = ($request->page) ? $request->page : 1;
        $slice = array_slice($attrs, $paginate * ($page - 1), $paginate);
		$lists = new LengthAwarePaginator($slice, count($attrs), $paginate, $page);
        $lists->setPath(action('HomeController@fetchQuestionnaire'));

        return response()->json($lists,200);


    }

    public function quiz()
    {
        return view('dashboard.quiz');
    }

    public function history()
    {
        $results = Test::paginate(5);
        return view('dashboard.history', compact('results'));
    }

    public function schools()
    {
        $provinces = \Indonesia::allProvinces();

        return view('dashboard.schools', compact('provinces'));
    }

    public function changePassword()
    {
        return view('dashboard.change-password');
    }

    public function terms()
    {
        return view('dashboard.terms');
    }

    public function result()
    {
        $dimensions = Dimension::where('is_active',true)->orderby('id','asc')->get();
        return view('dashboard.result',compact('dimensions'));
    }

    public function totalCountDimension(Request $request)
    {
        $dimensions = Dimension::where('is_active',true)->orderby('id','asc')->get();

        $array = array();
        foreach ($dimensions as $key => $dimension)
        {
            //auth::user()->id
            $total = $dimension->getTotal($dimension->id,auth::user()->id)[0]->total;
            array_push ($array,$total);

        }
        return response()->json($array,200);
    }

    public function totalCountIndicator(Request $request)
    {
        $indicators = Indicator::where('is_active','y')->orderby('dimension_id','asc')->get();

        $array = array();
        foreach ($indicators as $key => $indicator)
        {
            $total = $indicator->getTotal($indicator->id,auth::user()->id)[0]->total;
            array_push ($array,$total);

        }
        return response()->json($array,200);
    }

    public function policy()
    {
        return view('dashboard.policy');
    }

    public function storeAnswer(Request $request)
    {
        $data           = json_decode($request->data);
        $last_page      = $data->last_page;
        $questionnaires = json_decode(json_encode($data->data,true));

        $is_test_exists = Test::where([
            ['user_id',auth::user()->id],
            ['test_state','DRAFT'],
            ['type','FREE'],
        ])
        ->first();

        if(!$is_test_exists)
        {
            $test = Test::create([
                'user_id'       => auth::user()->id,
                'test_number'   => Test::generateTestNumber(),
                'test_state'    => 'DRAFT',
                'test_date'     => Carbon::now(),
                'type'          => 'FREE',
                'price'         => 0,
                'status'        => null,
                'paid_at'       => null,
            ]);
        }else $test = $is_test_exists;

        foreach ($questionnaires as $key => $value)
        {
           $is_answer_exists = TestQuestionnaire::where([
                ['questionnaire_id',$value->id],
                ['test_id',$test->id],
           ])
           ->first();

           if(!$is_answer_exists)
           {
                TestQuestionnaire::create([
                    'questionnaire_id'  => $value->id,
                    'test_id'           => $test->id,
                    'answer'            => $value->checked,
                ]);
           }else
           {
               $is_answer_exists->answer = $value->checked;
               $is_answer_exists->save();
           }
        }

        if($last_page)
        {
            $test->test_state = 'FINISH';
            $test->save();

            return response()->json([
                'redirect' => true,
                'url' => route('result')
            ]);
        }
    }

    public function updateAccount(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gender' => 'required',
            'age' => 'required',
            'education' => 'required',
            'nuptk' => 'required|numeric|digits:16',
            'nik' => 'required|numeric|digits:18',
            'employement_status' => 'required',
        ]);

        Auth::user()->update([
            'name' => $request->name,
            'gender' => $request->gender,
            'age_category_id' => $request->age,
            'education_id' => $request->education,
            'nuptk' => $request->nuptk,
            'nik' => $request->nik,
            'employement_status_id' => $request->employement_status,
        ]);

        return redirect()->back();
    }

    public function updateSchool(Request $request)
    {
        $request->validate([
            'school_level' => 'required',
            'status' => 'required',
            'school_name' => 'required',
            'province' => 'required',
            'city' => 'required',
            'teacher_type' => 'required',
            'subject' => 'required',
            'experience' => 'required',
            'certification' => 'required',
        ]);

        Auth::user()->update([
            'school_level_id' => $request->school_level,
            // 'school_status' => $request->status,
            'school_name' => $request->school_name,
            'province' => $request->province,
            'city' => $request->city,
            'teacher_type_id' => $request->teacher_type,
            'subject_id' => $request->subject,
            'years_experience_id' => $request->experience,
            'certification' => $request->certification,
            'additional_task_id' => $request->additional_task,
        ]);

        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
        if ( Hash::check($request->oldpassword, Auth::user()->password) ) {

            $request->validate([
                'oldpassword' => 'required',
                'password' => 'required|string|min:8|confirmed',
                'password_confirmation' => 'required|string|min:8',
            ],[
                'oldpassword.required' => 'Password lama wajib diisi',
                'password.required' => 'Password baru wajib diisi',
                'password.confirmed' => 'Konfirmasi password tidak cocok',
                'password_confirmation.required' => 'Konfirmasi ulang password baru Anda',
            ]);

            Auth::user()->update([
                'password' => bcrypt($request->password)
            ]);

            return redirect()->back();
        }
        else {
            return redirect()
                    ->back()
                    ->withErrors([
                        'oldpassword' => 'Password yang Anda masukan salah. Coba ulangi kembali'
                    ])
                    ->withInput();
        }
    }
}
