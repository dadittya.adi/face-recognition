<?php namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

use App\Models\Admin;
use Carbon\Carbon;
use Config;
use Mail;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.admin');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request){
        $email = $request->email;
        
        // var_dump($email);die;
        if ($email)
            $admin = Admin::where('email', '=', $email)
                ->first();

        if (!$email || !$admin) 
            return redirect()->action('Admin\Auth\LoginController@showLoginForm')
                ->with('_EMAIL_INVALID', true);

        $datetime = Carbon::now()->format('YmdHi');
        $token = base64_encode(hash_hmac('sha256', $datetime|$email, Config::get('app.reminder_key'), true));
        $token = strtr($token, ['/' => '_', '+' => '-', '=' => '~']);

        Mail::send('admin.email.forget_password', ['admin' => $admin, 'datetime' => $datetime, 'token' => $token], function($message) use ($admin)
        {
            $message->to($admin->email, $admin->name)->subject("Forget Password");
        });

        return redirect()->action('Admin\Auth\LoginController@showLoginForm')
            ->with('_SEND_FORGET_PASS.OK', true);
    }
}
