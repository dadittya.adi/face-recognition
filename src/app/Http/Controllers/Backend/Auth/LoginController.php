<?php namespace App\Http\Controllers\Backend\Auth;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;

use App\Models\Admin;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/backend/dashboard';

    public function __construct()
    {
       $this->middleware('guest.admin', ['except' => 'doLogout']);
    }
    

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }

    public function doLogin(Request $request)
    {
    	$this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $credentials = $this->credentials($request);
        if (Auth::guard('admins')->validate($credentials)) 
        {
            $admin = Auth::guard('admins')->getLastAttempted();
   
            if ($admin->email_verified_at) 
            {
                if(Auth::guard('admins')->attempt($request->only('email','password'),$request->filled('remember')))
                {
                    return redirect()->intended($this->redirectPath());
                }
                    
            } else 
            {
                Session::flash("flash_notification", [
                        "level"=>"info",
                        "message"=> "User belum aktif"
                ]);
                
                return  redirect()
                ->back()
                ->withInput($request->only($this->username(), 'remember'));
            }
        }

        return redirect('/backend/login')
        ->withInput($request->only('email', 'remember'))
        ->withErrors([
            'email' => 'Incorrect Email or password',
        ]);
    }
    
    public function doLogout(Request $request)
    {
        Auth::guard('admins')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
}
