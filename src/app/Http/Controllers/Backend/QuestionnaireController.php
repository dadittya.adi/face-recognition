<?php namespace App\Http\Controllers\Backend;

use DB;
use Excel;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Dimension;
use App\Models\Indicator;
use App\Models\Questionnaire;

class QuestionnaireController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        $dimensions     = Dimension::where('is_active','y')->pluck('name', 'id')->all();
        $indicators     = Indicator::where('is_active','y')->pluck('name', 'id')->all();
        $message        = $request->session()->get('message');
        return view('backend.questionnaire.index',compact('dimensions','indicators','message'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $indicator_id   = $request->indicator_id;
            $dimension_id   = $request->dimension_id;
            
            $data = db::select(db::raw("SELECT 
                    dimensions.id as dimension_id,
                    indicators.id as indicator_id,
                    questionnaires.id as questionnaire_id,
                    dimensions.name as dimension_name,
                    indicators.name as indicator_name,
                    questionnaires.name,
                    questionnaires.acronym,
                    questionnaires.pola_tc,
                    questionnaires.is_active
                FROM questionnaires
                JOIN indicators on  indicators.id = questionnaires.indicator_id and indicators.is_active = 'y'
                JOIN dimensions on  dimensions.id = indicators.dimension_id and dimensions.is_active = 'y'
                WHERE indicator_id::varchar like '%$indicator_id%'
                AND dimension_id::varchar like '%$dimension_id%'
                AND questionnaires.is_active = 'y'
            "));

            return datatables()->of($data)
            ->editColumn('is_active', function($data)
            {
                if(!$data->is_active == 'y') return '<span class="label label-default">not active</span>';
                else return '<span class="label label-success">active</span>';
            })
            ->addColumn('action', function($data) 
            {
                return view('backend.questionnaire._action', [
                    'model'     => $data,
                    'edit'      => route('backend.questionnaire.edit',$data->questionnaire_id),
                    'delete'    => route('backend.questionnaire.delete',$data->questionnaire_id),
                ]);
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
        }
    }

    public function import()
    {
        return view('backend.questionnaire.import');
    }

    public function downloadFormImport()
    {
        return Excel::create('upload_questionnaire',function ($excel)
        {
            $excel->sheet('active', function($sheet)
            {
                $sheet->setCellValue('A1','dimension');
                $sheet->setCellValue('B1','indicator');
                $sheet->setCellValue('C1','code_questionnaire');
                $sheet->setCellValue('D1','questionnaire');
                $sheet->setCellValue('E1','acronym');
                $sheet->setCellValue('F1','pola_tc');
               
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                ));
            });
        })
        ->export('xlsx');
    }

    public function uploadFormImport(Request $request)
    {
        $array  = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path                   = $request->file('upload_file')->getRealPath();
            $data                   = Excel::selectSheets('active')->load($path,function($render){})->get();

            try
            {
                DB::beginTransaction();

                Questionnaire::where('is_active','t')
                ->update([
                    'is_active' => 'y'
                ]);
                
                foreach ($data as $key => $value) 
                {
                    $dimension              = ($value->dimension ? trim(strtolower($value->dimension)) : null );
                    $indicator              = ($value->indicator ? trim(strtolower($value->indicator)) : null );
                    $code_questionnaire     = ($value->code_questionnaire ? trim(strtolower($value->code_questionnaire)) : null );
                    $questionnaire          = ($value->questionnaire ? trim(strtolower($value->questionnaire)) : null ); 
                    $acronym                = ($value->acronym ? trim(strtolower($value->acronym)) : null);
                    $pola_tc                = ($value->pola_tc ? trim(strtolower($value->pola_tc)) : null);
                    
                    if(
                        ($dimension != '' || $dimension != null) 
                        && ($indicator != null || $indicator != '') 
                        && ($code_questionnaire != null || $code_questionnaire != '' )
                        && ($questionnaire != null || $questionnaire != '' )
                        && ($acronym != null || $acronym != '' )
                        && ($pola_tc != null || $pola_tc != '' )
                    )
                    {
                        // 1. insert dimension
                        $is_dimesion_exists = Dimension::where([
                            [db::raw('lower(name)'),$dimension],
                            ['is_active','y'],
                        ])
                        ->first();

                        if($is_dimesion_exists)
                        {
                            $dimension_id = $is_dimesion_exists->id;
                        }else
                        {
                            $_dimension = Dimension::create([
                                'name'      => $dimension,
                                'is_active' => 'y',
                            ]);
                            //DB::commit();
                            $dimension_id = $_dimension->id;
                        }

                        // 2 insert indicator
                        $is_indicator_exists = Indicator::where([
                            ['dimension_id',$dimension_id],
                            [db::raw('lower(name)'),$indicator],
                            ['is_active','y'],
                        ])
                        ->first();

                        if($is_indicator_exists)
                        {
                            $indicator_id = $is_indicator_exists->id;
                        }else
                        {
                            $_indicator = Indicator::create([
                                'dimension_id'  => $dimension_id,
                                'name'          => $indicator,
                                'is_active'     => 'y',
                            ]);
                            //DB::commit();
                            $indicator_id = $_indicator->id;
                        }

                        // 3 insert questionnaire
                        $is_questionnaire_exists = Questionnaire::where([
                            ['indicator_id',$indicator_id],
                            [db::raw('lower(code)'),$code_questionnaire],
                            [db::raw('lower(name)'),$questionnaire],
                            [db::raw('lower(acronym)'),$acronym],
                            [db::raw('lower(pola_tc)'),$pola_tc],
                            ['is_active','y'],
                        ])
                        ->first();

                        if(!$is_questionnaire_exists)
                        {

                            Questionnaire::create([
                                'indicator_id'  => $indicator_id,
                                'code'          => $code_questionnaire,
                                'name'          => $questionnaire,
                                'acronym'       => $acronym,
                                'pola_tc'       => $pola_tc,
                                'order'         => substr($pola_tc,1,strlen($pola_tc)-2),
                                'option'        => substr($pola_tc,-1,1),
                                'is_active'     => 'y',
                            ]);

                            $obj                        = new stdClass();
                            $obj->dimension             = $dimension;
                            $obj->indicator             = $indicator;
                            $obj->code_questionnaire    = $code_questionnaire;
                            $obj->questionnaire         = $questionnaire;
                            $obj->acronym               = $acronym;
                            $obj->pola_tc               = $pola_tc;
                            $obj->is_error              = false;
                            $obj->upload_result         = 'Data berhasil disimpan';
                            $array []                   = $obj;
                        }else
                        {
                            $obj                        = new stdClass();
                            $obj->dimension             = $dimension;
                            $obj->indicator             = $indicator;
                            $obj->code_questionnaire    = $code_questionnaire;
                            $obj->questionnaire         = $questionnaire;
                            $obj->acronym               = $acronym;
                            $obj->pola_tc               = $pola_tc;
                            $obj->is_error              = true;
                            $obj->upload_result         = 'Data sudah ada';
                            $array []                   = $obj;
                        }
                    }else
                    {
                        $obj                        = new stdClass();
                        $obj->dimension             = $dimension;
                        $obj->indicator             = $indicator;
                        $obj->code_questionnaire    = $code_questionnaire;
                        $obj->questionnaire         = $questionnaire;
                        $obj->acronym               = $acronym;
                        $obj->pola_tc               = $pola_tc;
                        $obj->is_error              = true;
                        $obj->upload_result         = 'Cek kembali data anda';
                        $array []                   = $obj;
                    }
                }
                
                DB::commit();
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json($array,200);
                
        }
    }

    
    public function edit(Request $request,$id)
    {
        $data               = Questionnaire::find($id);
        $obj                = new StdClass();
        $obj->dimension_id  = $data->indicator->dimension_id;
        $obj->indicator_id  = $data->indicator_id;
        $obj->code          = $data->code;
        $obj->acronym       = $data->acronym;
        $obj->pola_tc       = $data->pola_tc;
        $obj->name          = $data->name;
        $obj->is_active     = ($data->is_active == 'y' ? 'active' : 'not-active');
        $obj->url_update    = route('backend.questionnaire.update',$data->id);

        return response()->json($obj,200);
    }

    public function update(Request $request, $id)
    {
        if(Questionnaire::where([
            ['id','!=',$id],
            ['indicator_id',$request->indicator],
            [db::raw('lower(name)'),strtolower($request->name)],
        ])->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        $data               = Questionnaire::find($id);
        $data->indicator_id = $request->indicator;
        $data->code         = strtolower($request->code);
        $data->name         = strtolower($request->name);
        $data->acronym      = strtolower($request->acronym);
        $data->pola_tc      = strtolower($request->pola_tc);
        $data->order        = substr($request->pola_tc,1,strlen($request->pola_tc)-2);
        $data->option       = substr($request->pola_tc,-1,1);
        $data->is_active    = ($request->is_active == 'active' ? 't' : 'f');
        $data->save();

        return response()->json(200);
    }

    public function delete($id)
    {
        $data = Questionnaire::find($id);
        if($data) $data->delete();

        return response()->json(200);
    }

    static function dataDimension(Request $request)
    {
        $q    = strtolower(trim($request->q));
        $data = Dimension::where([
            [db::raw('lower(name)'), "LIKE", "%$q%"],
            ['is_active', 'y'],
        ])
        ->get();

        return response()->json($data,200);
    }

    static function dataIndicator(Request $request)
    {
        $dimension_id   = $request->dimension_id;
        $q              = strtolower(trim($request->q));
        $data           = Indicator::where([
            ['dimension_id', "LIKE", "%$dimension_id%"],
            [db::raw('lower(name)'), "LIKE", "%$q%"],
            ['is_active', 'y'],
        ])
        ->get();

        return response()->json($data,200);
    }
}
