<?php namespace App\Http\Controllers\Backend;

use DB;
use StdClass;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\AgeCategory;

class AgeCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        return view('backend.age_category.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data       = AgeCategory::orderby('created_at','desc');
            
           
            return datatables()->of($data)
            ->editColumn('is_active', function($data)
            {
                if(!$data->is_active) return '<span class="label label-default">not active</span>';
                else return '<span class="label label-success">active</span>';
            })
            ->addColumn('action', function($data) {
                return view('backend.age_category._action', [
                    'model'     => $data,
                    'edit'      => route('backend.ageCategory.edit',$data->id),
                    'delete'    => route('backend.ageCategory.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        $data               = AgeCategory::find($id);
        $obj                = new StdClass();
        $obj->name          = $data->name;
        $obj->is_active     = ($data->is_active ? 'active' : 'not-active');
        $obj->url_update    = route('backend.ageCategory.update',$data->id);

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        if(AgeCategory::where(db::raw('lower(name)'),strtolower($request->name))->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        AgeCategory::create([
            'name'      => $request->name,
            'is_active' => ($request->is_active == 'active' ? true : false),
        ]);

        return response()->json(200);
    }

    public function update(Request $request,$id)
    {
        if(AgeCategory::where([
            ['id','!=',$id],
            [db::raw('lower(name)'),strtolower($request->name)],
        ])->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        $data               = AgeCategory::find($id);
        $data->name         = $request->name;
        $data->is_active    = ($request->is_active == 'active' ? true : false);
        $data->save();

        return response()->json(200);
    }

    public function delete(Request $request,$id)
    {
        $data = AgeCategory::find($id);
        if($data) $data->delete();

        return response()->json(200);
    }
}
