<?php namespace App\Http\Controllers\Backend;

use DB;
use StdClass;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\TeacherType;

class TeacherTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        return view('backend.teacher_type.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data       = TeacherType::orderby('created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('is_active', function($data)
            {
                if(!$data->is_active) return '<span class="label label-default">not active</span>';
                else return '<span class="label label-success">active</span>';
            })
            ->addColumn('action', function($data) {
                return view('backend.teacher_type._action', [
                    'model'     => $data,
                    'edit'      => route('backend.teacherType.edit',$data->id),
                    'delete'    => route('backend.teacherType.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        $data               = TeacherType::find($id);
        $obj                = new StdClass();
        $obj->name          = $data->name;
        $obj->is_active     = ($data->is_active ? 'active' : 'not-active');
        $obj->url_update    = route('backend.teacherType.update',$data->id);

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        if(TeacherType::where(db::raw('lower(name)'),strtolower($request->name))->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        TeacherType::create([
            'name'      => $request->name,
            'is_active' => ($request->is_active == 'active' ? true : false),
        ]);

        return response()->json(200);
    }

    public function update(Request $request,$id)
    {
        if(TeacherType::where([
            ['id','!=',$id],
            [db::raw('lower(name)'),strtolower($request->name)],
        ])->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        $data               = TeacherType::find($id);
        $data->name         = $request->name;
        $data->is_active    = ($request->is_active == 'active' ? true : false);
        $data->save();

        return response()->json(200);
    }

    public function delete(Request $request,$id)
    {
        $data = TeacherType::find($id);
        if($data) $data->delete();

        return response()->json(200);
    }
}
