<?php namespace App\Http\Controllers\Backend;

use DB;
use File;
use Config;
use StdClass;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\Album;
use App\Models\Gallery;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        $albums     = Album::pluck('title', 'id')->all();
        $message    = $request->session()->get('message');
        return view('backend.gallery.index',compact('albums','message'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $school_level   = $request->school_level;
            $data           = Gallery::where('album_id',"like","%$school_level%");
            
            return datatables()->of($data)
            ->editColumn('image',function ($data)
            {
               
                if ($data->image)
                {
                    $url = route('backend.gallery.showImage',$data->image);
                    return '<img src="'.$url.'" class="img-circle img-md" alt="'.$data->name.'">';
                } 
            	else return '';
            })
            ->addColumn('action', function($data) 
            {
                return view('backend.gallery._action', [
                    'model'     => $data,
                    'edit'      => route('backend.gallery.edit',$data->id),
                    'delete'    => route('backend.gallery.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','image'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        $albums     = Album::pluck('title', 'id')->all();
        $message    = $request->session()->get('message');
        return view('backend.gallery.create',compact('albums','message'));
    }

    public function edit(Request $request,$id)
    {
        $message    = $request->session()->get('message');
        $data       = Gallery::find($id);
        $albums     = Album::pluck('title', 'id')->all();

        if($data) return view('backend.gallery.edit',compact('data','albums'));
        else return redirect()->action('Backend\GalleryController@index');
    }

    public function store(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $description    = $request->description;
        $album_id       = $request->album;
        $storage        = Config::get('storage.gallery');
        $images         = $request->file('image');
        $_random        = null;
        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        foreach ($images as $key => $image) 
        {
            if($image)
            {
                $random = Gallery::generateFileName($image);
                $_random = $random[1];
              
                $gallery = Gallery::create([
                    'description'           => $description,
                    'album_id'              => $album_id,
                    'image'                 => $_random,
                ]);

                if ($gallery->save())
                {
                    $image->move($storage, $random[1]);
                }
            }
        }

        $request->session()->flash('message', 'success');
        return response()->json(200);
    }

    public function update(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $album_id       = $request->album;
        $description    = $request->description;
        $is_modify      = $request->is_modify;
        $storage        = Config::get('storage.gallery');
        $image          = $request->file('image');
        $_random        = null;
        $gallery        = Gallery::find($id);

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        if($image && $is_modify == 1)
        {
            $random = Gallery::generateFileName($image);
            $_random = $random[1];

            $old_file = $storage . '/' . $gallery->image;
            if(File::exists($old_file)) File::delete($old_file);
        }else
        {
            $_random = $gallery->image;
        }

        $gallery->album_id      = $album_id;
        $gallery->description   = $description;
        $gallery->image         = $_random;

        if ($gallery->save())
        {
            $request->session()->flash('message', 'success_2');
            if($image && $is_modify == 1) $image->move($storage, $_random);
        }
    }

    public function delete(Request $request,$id)
    {
        $gallery    = Gallery::find($id);
        $image      = $gallery->getImageFullPath($gallery->image);
        
        if(File::exists($image)) @unlink($image);
        $gallery->delete();

        return response()->json(200);
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.gallery');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    static function dataAlbum(Request $request)
    {
        $q    = strtolower(trim($request->q));
        $data = Album::where(db::raw('lower(title)'), "LIKE", "%$q%")->get();
        return response()->json($data,200);
    }

    
}
