<?php namespace App\Http\Controllers\Backend;

use DB;
use StdClass;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\Album;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        return view('backend.album.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data       = Album::orderby('created_at','desc');
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend.album._action', [
                    'model'     => $data,
                    'edit'      => route('backend.album.edit',$data->id),
                    'delete'    => route('backend.album.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        $data               = Album::find($id);
        $obj                = new StdClass();
        $obj->title          = $data->title;
        $obj->url_update    = route('backend.album.update',$data->id);

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        if(Album::where(db::raw('lower(title)'),strtolower($request->title))->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        Album::create([
            'title'      => $request->title
        ]);

        return response()->json(200);
    }

    public function update(Request $request,$id)
    {
        if(Album::where([
            ['id','!=',$id],
            [db::raw('lower(title)'),strtolower($request->title)],
        ])->exists()) return response()->json(['message' => 'Data '.$request->title.' already exists'],422);
        
        $data               = Album::find($id);
        $data->title        = $request->title;
        $data->save();

        return response()->json(200);
    }

    public function delete(Request $request,$id)
    {
        $data = Album::find($id);
        if($data) $data->delete();

        return response()->json(200);
    }
}
