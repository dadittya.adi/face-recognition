<?php namespace App\Http\Controllers\Backend;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\Admin;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        return view('backend.admin.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data       = Admin::where('id','!=',auth::guard('admins')->user()->id)
            ->orderby('updated_at','desc');
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend.admin._action', [
                    'model'     => $data,
                    'edit'      => route('backend.admin.edit',$data->id),
                    'delete'    => route('backend.admin.delete',$data->id),
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }
    

    public function edit(Request $request,$id)
    {
        $data               = Admin::find($id);
        $obj                = new StdClass();
        $obj->name          = $data->name;
        $obj->email         = $data->email;
        $obj->url_update    = route('backend.admin.update',$data->id);

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        if(Admin::where(db::raw('lower(email)'),strtolower($request->email))->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        Admin::create([
            'email'             => $request->email,
            'name'              => $request->name,
            'email_verified_at' => carbon::now(),
            'password'          => bcrypt($request->password),
        ]);

        return response()->json(200);
    }

    public function update(Request $request,$id)
    {
        if(Admin::where([
            ['id','!=',$id],
            [db::raw('lower(email)'),strtolower($request->email)],
        ])->exists()) return response()->json(['message' => 'Data '.$request->name.' already exists'],422);
        
        $data               = Admin::find($id);
        $data->email        = $request->email;
        $data->name         = $request->name;
        if($request->is_modify == 1)  $data->password = bcrypt($request->password);
        $data->save();

        return response()->json(200);
    }

    public function delete(Request $request,$id)
    {
        $data = Admin::find($id);
        if($data) $data->delete();

        return response()->json(200);
    }
}
