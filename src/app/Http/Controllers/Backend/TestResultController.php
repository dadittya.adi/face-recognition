<?php namespace App\Http\Controllers\Backend;

use DB;
use Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Questionnaire;
use App\Models\TestQuestionnaire;

class TestResultController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth.admin');
    }


    public function index()
    {
        return view('backend.test_result.index');
    }

   
    public function downloadReport()
    {
        $columns                = array('test_id','tanggal_test','nomor_test','nama','email','jenis_kelamin','usia','latar_belakang_pendidikan','nuptk','nik','status_kepegawaian','jenjang_sekolah','nama_sekolah','status_sekolah','jenis_guru','mata_pelajaran_utama','lama_mengajar','sertifikasi','tugas_tambahan','propinsi','kota');
        $total_questionnaire    = Questionnaire::count();
        $alphabets              = array(
            0 => 'A',
            1 => 'B',
            2 => 'C',
            3 => 'D',
            4 => 'E',
            5 => 'F',
            6 => 'G',
            7 => 'H',
            8 => 'I',
            9 => 'J',
            10 => 'K',
            11 => 'L',
            12 => 'M',
            13 => 'N',
            14 => 'O',
            15 => 'P',
            16 => 'Q',
            17 => 'R',
            18 => 'S',
            19 => 'T',
            20 => 'U',
            21 => 'V',
            22 => 'W',
            23 => 'X',
            24 => 'Y',
            25 => 'Z',

            26 => 'AA',
            27 => 'AB',
            28 => 'AC',
            29 => 'AD',
            30 => 'AE',
            31 => 'AF',
            32 => 'AG',
            33 => 'AH',
            34 => 'AI',
            35 => 'AJ',
            36 => 'AK',
            37 => 'AL',
            38 => 'AM',
            39 => 'AN',
            40 => 'AO',
            41 => 'AP',
            42 => 'AQ',
            43 => 'AR',
            44 => 'AS',
            45 => 'AT',
            46 => 'AU',
            47 => 'AV',
            48 => 'AW',
            49 => 'AX',
            50 => 'AY',
            51 => 'AZ',

            52 => 'BA',
            53 => 'BB',
            54 => 'BC',
            55 => 'BD',
            56 => 'BE',
            57 => 'BF',
            58 => 'BG',
            59 => 'BH',
            60 => 'BI',
            61 => 'BJ',
            62 => 'BK',
            63 => 'BL',
            64 => 'BM',
            65 => 'BN',
            66 => 'BO',
            67 => 'BP',
            68 => 'BQ',
            69 => 'BR',
            70 => 'BS',
            71 => 'BT',
            72 => 'BU',
            73 => 'BV',
            74 => 'BW',
            75 => 'BX',
            76 => 'BY',
            77 => 'BZ',

            78 => 'CA',
            79 => 'CB',
            80 => 'CC',
            81 => 'CD',
            82 => 'CE',
            83 => 'CF',
            84 => 'CG',
            85 => 'CH',
            86 => 'CI',
            87 => 'CJ',
            88 => 'CK',
            89 => 'CL',
            90 => 'CM',
            91 => 'CN',
            92 => 'CO',
            93 => 'CP',
            94 => 'CQ',
            95 => 'CR',
            96 => 'CS',
            97 => 'CT',
            98 => 'CU',
            99 => 'CV',
            100 => 'CW',
            101 => 'CX',
            102 => 'CY',
            103 => 'CZ',

            104 => 'DA',
            105 => 'DB',
            106 => 'DC',
            107 => 'DD',
            108 => 'DE',
            109 => 'DF',
            110 => 'DG',
            111 => 'DH',
            112 => 'DI',
            113 => 'DJ',
            114 => 'DK',
            115 => 'DL',
            116 => 'DM',
            117 => 'DN',
            118 => 'DO',
            119 => 'DP',
            120 => 'DQ',
            121 => 'DR',
            122 => 'DS',
            123 => 'DT',
            124 => 'DU',
            125 => 'DV',
            126 => 'DW',
            127 => 'DX',
            128 => 'DY',
            129 => 'DZ',

            130 => 'EA',
            131 => 'EB',
            132 => 'EC',
            133 => 'ED',
            134 => 'EE',
            135 => 'EF',
            136 => 'EG',
            137 => 'EH',
            138 => 'EI',
            139 => 'EJ',
            140 => 'EK',
            141 => 'EL',
            142 => 'EM',
            143 => 'EN',
            144 => 'EO',
            145 => 'EP',
            146 => 'EQ',
            147 => 'ER',
            148 => 'ES',
            149 => 'ET',
            150 => 'EU',
            151 => 'EV',
            152 => 'EW',
            153 => 'EX',
            154 => 'EY',
            155 => 'EZ',

            156 => 'FA',
            157 => 'FB',
            158 => 'FC',
            159 => 'FD',
            160 => 'FE',
            161 => 'FF',
            162 => 'FG',
            163 => 'FH',
            164 => 'FI',
            165 => 'FJ',
            166 => 'FK',
            167 => 'FL',
            168 => 'FM',
            169 => 'FN',
            170 => 'FO',
            171 => 'FP',
            172 => 'FQ',
            173 => 'FR',
            174 => 'FS',
            175 => 'FT',
            176 => 'FU',
            177 => 'FV',
            178 => 'FW',
            179 => 'FX',
            180 => 'FY',
            181 => 'FZ',

            182 => 'GA',
            183 => 'GB',
            184 => 'GC',
            185 => 'GD',
            186 => 'GE',
            187 => 'GF',
            188 => 'GG',
            189 => 'GH',
            190 => 'GI',
            191 => 'GJ',
            192 => 'GK',
            193 => 'GL',
            194 => 'GM',
            195 => 'GN',
            196 => 'GO',
            197 => 'GP',
            198 => 'GQ',
            199 => 'GR',
            200 => 'GS',
            201 => 'GT',
            202 => 'GU',
            203 => 'GV',
            204 => 'GW',
            205 => 'GX',
            206 => 'GY',
            207 => 'GZ',
        );

        $data                   = DB::select(db::raw("
            select tests.id as test_id,
            tests.test_date as tanggal_test,
            tests.test_number as nomor_test,
            users.name as nama,
            users.email,
            users.gender as jenis_kelamin,
            age_categories.name as usia,
            educations.name as latar_belakang_pendidikan,
            users.nuptk,
            users.nik,
            employement_status.name as status_kepegawaian,
            school_levels.name as jenjang_sekolah,
            users.school_name as nama_sekolah,
            users.school_status as status_sekolah,
            teacher_types.name as jenis_guru,
            subjects.name as mata_pelajaran_utama,
            years_experience.name as lama_mengajar,
            users.certification as sertifikasi,
            additional_tasks.name as tugas_tambahan,
            users.province as propinsi,
            users.city as kota
            from tests
            join users on users.id = tests.user_id
            left join age_categories on age_categories.id = users.age_category_id
            left join educations on educations.id = users.education_id
            left join employement_status on employement_status.id = users.employement_status_id
            left join school_levels on school_levels.id = users.school_level_id
            left join subjects on subjects.id = users.subject_id 
            left join years_experience on years_experience.id = users.years_experience_id
            left join teacher_types on teacher_types.id = users.teacher_type_id
            left join additional_tasks on additional_tasks.id = users.additional_task_id
            order by users.id asc
        "));

        for ($i=0; $i < $total_questionnaire; $i++) array_push($columns, 'test|b'.($i+1));
        
        return Excel::create('report_test_questionnaire',function ($excel) use ($columns,$alphabets,$data)
        {
            $excel->sheet('active', function($sheet) use ($columns,$alphabets,$data)
            {
                foreach ($columns as $key => $value) 
                {
                    $is_test_answer = strpos($value, '|');

                    if($is_test_answer == false) $column_value = $value;
                    else
                    {
                        $split          = explode('|',$value);
                        $code           = $split[1];
                        $column_value   = $code;
                    }
                    
                    $sheet->setCellValue($alphabets[$key].'1',($column_value == 'test_id' ? 'no' : $column_value));

                    $j = 2;
                    foreach ($data as $key_2 => $datum) 
                    {
                        $test_id        = $datum->test_id;
                        $row_value      = null;

                        if($value != 'test_id')
                        {
                            if($is_test_answer == false)
                            {
                                $row_value = $datum->$value;
                            } 
                            else
                            {
                                $split              = explode('|',$value);
                                $code               = $split[1];
                                
                                $test_questionnaire = TestQuestionnaire::where('test_id',$test_id)
                                ->whereHas('questionnaire',function($query) use ($code)
                                {
                                    $query->where('code',$code);
                                })
                                ->first();

                                $row_value      = ($test_questionnaire ? ($test_questionnaire->answer ? 1 : 0) : null);
                            }
                        } else $row_value = $key_2+1;

                        $sheet->setCellValue($alphabets[$key].$j,$row_value);
                        $j++; 
                    }
                }
                
            });
        })
        ->export('xlsx');
    }


}
