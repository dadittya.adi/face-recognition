<?php namespace App\Http\Controllers\Backend;

use DB;
use StdClass;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\Subject;
use App\Models\SchoolLevel;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        $school_levels = SchoolLevel::where('is_active',true)->pluck('name', 'id')->all();
        return view('backend.subject.index',compact('school_levels'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $school_level   = $request->school_level;
            $data           = Subject::where('school_level_id',"like","%$school_level%");
            
            return datatables()->of($data)
            ->editColumn('is_active', function($data)
            {
                if(!$data->is_active) return '<span class="label label-default">not active</span>';
                else return '<span class="label label-success">active</span>';
            })
            ->addColumn('action', function($data) {
                return view('backend.school_level._action', [
                    'model'     => $data,
                    'edit'      => route('backend.subject.edit',$data->id),
                    'delete'    => route('backend.subject.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        $data                   = Subject::find($id);
        $obj                    = new StdClass();
        $obj->name              = $data->name;
        $obj->school_level_id   = $data->school_level_id;
        $obj->is_active         = ($data->is_active ? 'active' : 'not-active');
        $obj->url_update        = route('backend.subject.update',$data->id);

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $school_level = SchoolLevel::find($request->school_level);
        if(Subject::where([
            [db::raw('lower(name)'),strtolower($request->name)],
            ['school_level_id',$school_level->id],
        ])->exists()) return response()->json(['message' => 'Data '.$request->name.' for school level '.( $school_level ? $school_level->name : null).' already exists'],422);
        
        Subject::create([
            'name'                  => $request->name,
            'school_level_id'       => $school_level->id,
            'is_active'             => ($request->is_active == 'active' ? true : false),
        ]);

        return response()->json(200);
    }

    public function update(Request $request,$id)
    {
        $school_level = SchoolLevel::find($request->school_level);
        if(Subject::where([
            ['id','!=',$id],
            ['school_level_id',$school_level->id],
            [db::raw('lower(name)'),strtolower($request->name)],
        ])->exists()) return response()->json(['message' => 'Data '.$request->name.' for school level '.( $school_level ? $school_level->name : null).' already exists'],422);
        
        $data                       = Subject::find($id);
        $data->name                 = $request->name;
        $data->school_level_id      = $school_level->id;
        $data->is_active            = ($request->is_active == 'active' ? true : false);
        $data->save();

        return response()->json(200);
    }

    public function delete(Request $request,$id)
    {
        $data = Subject::find($id);
        if($data) $data->delete();

        return response()->json(200);
    }

    static function dataSchoolLevel(Request $request)
    {
        $q    = strtolower(trim($request->q));
        $data = SchoolLevel::where(db::raw('lower(name)'), "LIKE", "%$q%")->where('is_active',true)->get();
        return response()->json($data,200);
    }
}
