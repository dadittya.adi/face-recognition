<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

use App\Models\Articles;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        $message    = $request->session()->get('message');
        return view('backend.article.index',compact('message'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data           = Articles::orderby('updated_at','desc');
            return datatables()->of($data)
            ->editColumn('is_active',function ($data)
            {
                if ($data->is_active) return '<span class="label label-flat label-rounded label-icon border-success text-success-600"><i class="icon-checkmark3"></i></span>';
            	else return '<span class="label label-flat label-rounded label-icon border-grey text-grey-600""><i class="icon-warning"></i></span>';
            })
            ->editColumn('image',function ($data)
            {
               
                if ($data->image)
                {
                    $url = route('backend.article.showImage',$data->image);
                    return '<img src="'.$url.'" class="img-circle img-md" alt="'.$data->name.'">';
                } 
            	else return '';
            })
            ->addColumn('action', function($data) {
                return view('backend.article._action', [
                    'model'     => $data,
                    'edit'      => route('backend.article.edit',$data->id),
                    'delete'    => route('backend.article.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active','image'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('backend.article.create');
    }
    
    public function store(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $title      = $request->title;
        $content    = $request->content;
        $is_active  = ($request->exists('is_active') ? true:false);
        $storage    = Config::get('storage.article');
        $image      = $request->file('image');
        $_random    = null;

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        if($image)
        {
            $random         = Articles::generateFileName($image);
            $_random        = $random[1];

            $image_mimetype = $image->getMimeType(); 
            $image_resize   = Image::make($image->getRealPath());
            $width          = $image_resize->width();
            $height         = $image_resize->height();
            $newWidth       = 700;
            $diff           = $width/$newWidth;
            $newHeight      = $height/$diff;

            $image_resize->resize($newWidth, $newHeight);
        }

        $total_title = Articles::where('slug',str_slug($title))->count();
        $article = Articles::create([
            'title'         => $title,
            'slug'          => str_slug($title.$total_title),
            'thumbnail'     => 'thumbnail_'.$_random,
            'image'         => $_random,
            'content'       => $content,
            'is_active'     => $is_active,
            'admin_id'      => auth::guard('admins')->user()->id,
        ]);

        if ($article->save())
        {
            $image->move($storage, $random[1]);
            $image_resize->save($storage.'/'.'thumbnail_'.$random[1]);
        }

        $request->session()->flash('message', 'success');
        return response()->json('success',200);
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.article');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function edit(Request $request,$id)
    {
        $message    = $request->session()->get('message');
        $data       = Articles::find($id);

        if($data) return view('backend.article.edit',compact('data'));
        else return redirect()->action('Backend\ArticleController@index');
    }

    public function update(Request $request, $id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $title      = $request->title;
        $content    = $request->content;
        $is_modify  = $request->is_modify;
        $is_active  = ($request->exists('is_active') ? true:false);
        $storage    = Config::get('storage.article');
        $image      = $request->file('image');
        $_random    = null;
        $article    = Articles::find($id);

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        if($image && $is_modify == 1)
        {
            $random         = Articles::generateFileName($image);
            $_random        = $random[1];

            $image_mimetype = $image->getMimeType(); 
            $image_resize   = Image::make($image->getRealPath());
            $width          = $image_resize->width();
            $height         = $image_resize->height();
            $newWidth       = 700;
            $diff           = $width/$newWidth;
            $newHeight      = $height/$diff;

            $image_resize->resize($newWidth, $newHeight);

            $old_file_1 = $storage . '/' . $article->image;
            $old_file_2 = $storage . '/thumbnail_' . $article->image;
            if(File::exists($old_file_1)) File::delete($old_file_1);
            if(File::exists($old_file_2)) File::delete($old_file_2);
        }else
        {
            $_random = $article->image;
        }
        
        $total_title = Articles::where([
            ['id','!=',$id],
            ['slug',str_slug($title)],
        ])->count();

        $article->slug          = str_slug($title.$total_title);
        $article->title         = $title;
        $article->content       = $content;
        $article->image         = $_random;
        $article->thumbnail     = 'thumbnail_'.$_random;
        $article->is_active     = $is_active;

        if ($article->save())
        {
            $request->session()->flash('message', 'success_2');
            if($image && $is_modify == 1)
            {
                $image->move($storage, $_random);
                $image_resize->save($storage.'/'.'thumbnail_'.$_random);
            } 
        }
    }

    public function delete($id)
    {
        $article    = Articles::find($id);
        $image      = $article->getImageFullPath($article->image);
        
        if(File::exists($image)) @unlink($image);

        $article->delete();
    }
}
